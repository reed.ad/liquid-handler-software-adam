import json
from opentrons.jsonParse import CommandParser, LabwareParser, WellParser
from hal.liquidHandlerHal import LiquidHandlerHal
import threading

LastLabware = None

# Enable disable compatibility with opentrons deck
OPENTRONS_COMPATIBLE = True

class LiquidHandlerIal:

    def __init__(self):
        self.ApplicationHandler = LiquidHandlerHal()
        self._stop_event = threading.Event()
        self._execution_thread = threading.Thread()

    """Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates' and 'z_coordinates',
    then performs a pick up tip action. The sequence of movement is as follows: 
    1: Positions z axis at 'travel hight' which is configured in the config file.
    2. Move axis x and y to given coordinates and wait thill the movement is over
    3. Move axis z to position for picking up a tip
    4. Move axis z above the well and wait till movement is over
    
    switchLabware - defines if the action is about to move to another labware
    wellDepth - not used in current implementation"""
    def pickupTip(self, x_coordinates, y_coordinates, z_coordinates, wellDepth, switchLabware = False):
        try:
            #set Led Strip to indicate movemennt
            self.ApplicationHandler.LedSetColor("Running")
            travelHightOffset = self.ApplicationHandler.GetTravelHightOffset('z')
            print("=> pickupTip")
            
            # Calculate travel hight
            travelHight = 0
            if (switchLabware == False):
                travelHight = z_coordinates - travelHightOffset

            # Move Axis Z above the well
            self.ApplicationHandler.MoveAxis('z', travelHight, True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            # Move Axis X and Y to desired positions 
            self.ApplicationHandler.MoveAxis('x', x_coordinates)
            print(f"=> move X : {x_coordinates}")
            self.ApplicationHandler.MoveAxis('y', y_coordinates, True)
            print(f"=> move Y : {y_coordinates}")

            # TODO: set correct z coordinates for pick up tip
            # Move Axis Z to possition for picking up the tip
            self.ApplicationHandler.MoveAxis('z', z_coordinates, True)
            print(f"=> move Z : {z_coordinates}")

            #TODO: pickup the tip
            print("=> TODO: pickupTip")
            self.ApplicationHandler.MoveAxis('z', z_coordinates + 9, True)
            print(f"=> pick up tip Z : {z_coordinates} + 9mm")

            # Move Axis Z back above the well
            self.ApplicationHandler.MoveAxis('z', (z_coordinates - travelHightOffset), True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            #set Led Strip to Festo Color
            self.ApplicationHandler.LedSetColor("Idle")
        except Exception as e:
            #Set error to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Error")
            print("Warning: Something went wrong in 'pickupTip()': ", str(e))

    """Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates' and 'z_coordinates',
    then performs a drop tip action. The sequence of movement is as follows: 
    1: Positions z axis at 'travel hight' which is configured in the config file.
    2. Move axis x and y to given coordinates and wait thill the movement is over
    3. Move axis z to position for drop a tip
    4. Move axis z above the well and wait till movement is over
    
    switchLabware - defines if the action is about to move to another labware
    wellDepth - not used in current implementation"""
    def dropTip(self, x_coordinates, y_coordinates, z_coordinates, wellDepth, switchLabware = False):
        try:
            #Set movement to LED strip
            self.ApplicationHandler.LedSetColor("Running")

            travelHightOffset = self.ApplicationHandler.GetTravelHightOffset('z')
            print("=> dropTip")

            # Calculate travel hight
            travelHight = 0
            if (switchLabware == False):
                travelHight = z_coordinates - travelHightOffset

            # Move Axis Z above the well
            self.ApplicationHandler.MoveAxis('z', travelHight, True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            # Move Axis X and Y to desired positions 
            self.ApplicationHandler.MoveAxis('x', x_coordinates)
            print(f"=> move X : {x_coordinates}")
            self.ApplicationHandler.MoveAxis('y', y_coordinates, True)
            print(f"=> move Y : {y_coordinates}")

            # TODO: set correct z coordinates for drop tip
            # Move Axis Z to possition for dropping up the tip
            self.ApplicationHandler.MoveAxis('z', z_coordinates, True)
            print(f"=> move Z : {z_coordinates}")

            #TODO: drop the tip
            print("=> TODO: dropTip")

            # Move Axis Z back above the well
            self.ApplicationHandler.MoveAxis('z', (z_coordinates - travelHightOffset), True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            #Set Festo to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Idle")

        except Exception as e:
            #Set error to LED strip
            self.ApplicationHandler.LedSetColor("Error")

            print("Warning: Something went wrong in 'dropTip()': ", str(e))

    """Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates', 'z_coordinates',
    'wellDepth' and 'offsetFromBottomMm' then performs a aspirate with liquid volume given by 'liquidVolume'. The sequence of movement is as follows: 
    1: Positions z axis at 'travel hight' which is configured in the config file.
    2. Move axis x and y to given coordinates and wait thill the movement is over
    3. Move axis z to position for aspirate
    4. Move axis z above the well and wait till movement is over
    
    liquidVolume - liquid volume to aspirate
    wellDepth - defines the depth of the well needed for calculation of z axis movement
    offsetFromBottomMm - possition for the z axis to be set in respect of offset from the bottom
    switchLabware - defines if the action is about to move to another labware"""
    def aspirate(self, x_coordinates, y_coordinates, z_coordinates, liquidVolume, wellDepth, offsetFromBottomMm ,switchLabware = False):
        try:
            #Set dynamic to LED strip
            self.ApplicationHandler.LedSetColor("Running")

            travelHightOffset = self.ApplicationHandler.GetTravelHightOffset('z')
            print("=> aspirate")

            # Calculate travel hight
            travelHight = 0
            if (switchLabware == False):
                travelHight = z_coordinates - travelHightOffset

            # Move Axis Z above the well
            self.ApplicationHandler.MoveAxis('z', travelHight , True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            # Move Axis X and Y to desired positions 
            self.ApplicationHandler.MoveAxis('x', x_coordinates)
            print(f"=> move X : {x_coordinates}")
            self.ApplicationHandler.MoveAxis('y', y_coordinates, True)
            print(f"=> move Y : {y_coordinates}")

            # Move Axis Z to possition for aspirate
            self.ApplicationHandler.MoveAxis('z', (z_coordinates + wellDepth - offsetFromBottomMm), True)
            print(f"=> move Z : {z_coordinates}")

            self.ApplicationHandler.Aspirate(liquidVolume)
            print(f"=> : aspirate {liquidVolume}")

            # Move Axis Z back above the well
            self.ApplicationHandler.MoveAxis('z', (z_coordinates - travelHightOffset), True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            #Set Festo to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Idle")

        except Exception as e:
            #Set error to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Error")

            print("Warning: Something went wrong in 'aspirate()': ", str(e))

    """Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates', 'z_coordinates',
    'wellDepth' and 'offsetFromBottomMm' then performs a dispense with liquid volume given by 'liquidVolume'. The sequence of movement is as follows: 
    1: Positions z axis at 'travel hight' which is configured in the config file.
    2. Move axis x and y to given coordinates and wait thill the movement is over
    3. Move axis z to position for dispense
    4. Move axis z above the well and wait till movement is over
    
    liquidVolume - liquid volume to dispense
    wellDepth - defines the depth of the well needed for calculation of z axis movement
    offsetFromBottomMm - possition for the z axis to be set in respect of offset from the bottom
    switchLabware - defines if the action is about to move to another labware"""
    def dispense(self, x_coordinates, y_coordinates, z_coordinates, liquidVolume, wellDepth, offsetFromBottomMm, switchLabware = False):
        try:
            #Set dynamic to LED strip
            self.ApplicationHandler.LedSetColor("Running")

            travelHightOffset = self.ApplicationHandler.GetTravelHightOffset('z')
            print("=> deispense")

            # Calculate travel hight
            travelHight = 0
            if (switchLabware == False):
                travelHight = z_coordinates - travelHightOffset

            # Move Axis Z above the well
            self.ApplicationHandler.MoveAxis('z', travelHight, True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            # Move Axis X and Y to desired positions 
            self.ApplicationHandler.MoveAxis('x', x_coordinates)
            print(f"=> move X : {x_coordinates}")
            self.ApplicationHandler.MoveAxis('y', y_coordinates, True)
            print(f"=> move Y : {y_coordinates}")

            # Move Axis Z to possition for dispense
            self.ApplicationHandler.MoveAxis('z', (z_coordinates + wellDepth - offsetFromBottomMm), True)
            print(f"=> move Z : {z_coordinates}")

            self.ApplicationHandler.Dispense(liquidVolume)
            print(f": dispense : {liquidVolume}")

            # Move Axis Z back above the well
            self.ApplicationHandler.MoveAxis('z', (z_coordinates - travelHightOffset), True)
            print(f"=> move Z : {(z_coordinates - travelHightOffset)}")

            #Set Festo to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Idle")

        except Exception as e:
            #Set error to LED strip
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Error")

            print("Warning: Something went wrong in 'dispense()': ", str(e))

    """For internal use, picks up a correct command by 'commandType' and makes a call with the respective coordinates"""
    def executeCommand(self, commandType : str, liquidVolume:int, absolutePosition:object, wellDepth, offsetFromBottomMm:float, switchLabware:bool):
        try:
            absolute_x = absolutePosition['x']
            absolute_y = absolutePosition['y']
            absolute_z = absolutePosition['z']

            if commandType == 'pickUpTip':
                self.pickupTip(absolute_x, absolute_y, absolute_z, wellDepth, switchLabware)

            elif commandType == 'dropTip':
                self.dropTip(absolute_x, absolute_y, absolute_z, wellDepth, switchLabware)

            elif commandType == 'aspirate':
                self.aspirate(absolute_x, absolute_y, absolute_z, liquidVolume, wellDepth, offsetFromBottomMm, switchLabware)

            elif commandType == 'dispense':
                self.dispense(absolute_x, absolute_y, absolute_z, liquidVolume, wellDepth, offsetFromBottomMm, switchLabware)

        except Exception as e:
            print("Warning: Something went wrong in 'executeCommand()': ", str(e))
            
    """For internal use. Extract all the needed data from given handlers for labware and commands, constructs absolute coordinates,
    based on given slot and prepare data for command execution"""
    def constructCommand(self, labwareH, commandH, commandNum):
        try:
            global LastLabware
            print(f"====================================================== {commandNum} ======================================================")
            # Get command well
            commandWell = commandH.getWell(commandNum)
            # Get labware id
            wabId = commandH.getLabware(commandNum)
            # Get well settings
            wellSettings = labwareH.getWellSettings(wabId, commandWell)
            # Get labware slot
            labSlot = int(labwareH.getSlot(wabId))
            # Get well handler
            wellH = WellParser(wellSettings)
            # Get X, Y and Z
            if (OPENTRONS_COMPATIBLE == True):
                well_x = wellH.getPositionY()
                well_y = wellH.getPositionX()
            else:
                well_x = wellH.getPositionX()
                well_y = wellH.getPositionY()
            well_z = wellH.getPositionZ()
            # Get well depth
            depth = wellH.getDepth()
            # Get command Type
            commandType = commandH.getType(commandNum)
            # Get liquid volume
            liquidVolume = commandH.getVolume(commandNum)
            # Get offset from the bottom
            offsetFromBottomMm = commandH.getOffsetFromBottom(commandNum)
            # Get labware display name
            displayName = labwareH.getDisplayName(wabId)
            # Get volume units
            volumeUnits = labwareH.getDisplayVolumeUnits(wabId)
            # Get labware XYZ
            labwareXYZ = labwareH.getDimensionsXYZ(wabId)
            print(f"Labware dimentions: x:{labwareXYZ[0]}, y:{labwareXYZ[1]}, z:{labwareXYZ[2]}")
            # Get wabware
            labware = commandH.getLabware(commandNum)
            # Check if labware is switched
            switchLabware = False
            if (LastLabware != labware):
                switchLabware = True
                LastLabware = labware    

            print("Slot: ", labSlot)
            # Add starting point coordinates
            if (OPENTRONS_COMPATIBLE == True):
                # Calculate absolute coordinates
                absolute_x, absolute_y = self.calculateAbsoluteXYZ_opentrons(labSlot, well_x, well_y)
                print(f"Opentrons: x: {absolute_x}, y: {absolute_y}")
                if (absolute_x is not None and absolute_y is not None):
                    absolute_x = self.ApplicationHandler.GetAxisStaringPoint('x') + absolute_x
                    absolute_y = self.ApplicationHandler.GetAxisStaringPoint('y') - absolute_y
                else:
                    raise Exception("Absolute x and y coordinates are note available !")
                if (commandType == 'dropTip'):
                    absolute_z = self.ApplicationHandler.GetAxisStaringPoint('z') - well_z
                else:
                    absolute_z = self.ApplicationHandler.GetAxisStaringPoint('z') + well_z
            else:
                absolute_x, absolute_y = self.calculateAbsoluteXYZ(labSlot, well_x, well_y)
                if (absolute_x is not None and absolute_y is not None):
                    absolute_x = self.ApplicationHandler.GetAxisStaringPoint('x') + absolute_x
                    absolute_y = self.ApplicationHandler.GetAxisStaringPoint('y') + absolute_y
                else:
                    raise Exception("Absolute x and y coordinates are note available !")
                absolute_z = self.ApplicationHandler.GetAxisStaringPoint('z') + well_z
            # TODO: Check if well X, Y and Z are absolute coordinates or relative to the labwere

            if liquidVolume is not None:
                print(f"{commandNum}: {commandType} volume {liquidVolume} {volumeUnits} from labware '{displayName}' form well {commandWell} at [x:{absolute_x}, y:{absolute_y}, z:{absolute_z}]")
            else:
                print(f"{commandNum}: {commandType} from labware '{displayName}' from well {commandWell} at [x:{absolute_x}, y:{absolute_y}, z:{absolute_z}]")

            print(f"Labware switch: {switchLabware}")

            absolutePosition = {'x': absolute_x,
                                'y': absolute_y,
                                'z': absolute_z}

            if offsetFromBottomMm is not None:
                self.executeCommand(commandType, liquidVolume, absolutePosition, depth, float(offsetFromBottomMm), switchLabware)
            else:
                self.executeCommand(commandType, liquidVolume, absolutePosition, depth, 0.0, switchLabware)

        except Exception as e:
            print("Warning: Something went wrong in 'constructCommand()': ", str(e))

    """Calculating the absolute coordinates based on slot number and relative x and y axises coordinates"""
    def calculateAbsoluteXYZ(self, slotNum:int, relX:int, relY:int):
        try:
            slotRowIndex = 0
            slotColIndex = slotNum

            RowsNum, ColsNum = self.ApplicationHandler.GetDeckRowsColumns()
            slotSize_X, slotSize_Y = self.ApplicationHandler.GetSlotSizeXY()

            slotSize_X = slotSize_X + self.ApplicationHandler.GetSlotBorderThicknessX()
            slotSize_Y = slotSize_Y + self.ApplicationHandler.GetSlotBorderThicknessY()

            if (0 < slotNum and slotNum <= (RowsNum * ColsNum)):
                #Get correct index of the slot
                for i in range(ColsNum):
                    if slotColIndex <= ColsNum:
                        break
                    else:
                        slotColIndex = slotColIndex - ColsNum
                        slotRowIndex = slotRowIndex + 1

                slotColIndex = slotColIndex - 1

                # Calculate offset by X and Y axises depending on the indexes
                offsetX = slotRowIndex * slotSize_X
                offsetY = slotColIndex * slotSize_Y

                # Calculate absolute values
                offsetX = relX + offsetX
                offsetY = relY + offsetY
            else:
                print(f"Wrong slot number'{slotNum}', slot should be between 0 and {RowsNum * ColsNum}")
                return None

        except Exception as e:
            offsetX = offsetY = None
            print(f"Warning: Something went wrong in 'CalculateAbsoluteXYZ({slotNum}, {relX}, {relY})': ", str(e))

        return offsetX, offsetY

    """Calculating the absolute coordinates based on slot number and relative x and y axises coordinates
    specifically for Opentrons plate design"""
    def calculateAbsoluteXYZ_opentrons(self, labSlot: int, relX: int, relY: int):
        """Labware map (Opentrons)
        - slots[4][3]:

             /10/11/12/       z(+) / y (+)
            / 7/ 8/ 9/         |  /
           / 4/ 5/ 6/          | /
          / 1/ 2/ 3/   (0,0,0) |------- x(+)
                                  front
        slot 12 - Trash
        """
        #TODO: set real values
        slotWidth = 86 + 4.5 #mm; <-- x -->
        slotLength = 128 + 4.5 #mm; <-- y -->

        try:
            # First row
            if (labSlot == 1):
                pass
            elif (labSlot == 2):
                # relX = relX + slotWidth
                relY = relY + slotLength
            elif (labSlot == 3):
                # relX = relX + (slotWidth * 2)
                relY = relY + (slotLength * 2)
            # Second row
            elif (labSlot == 4):
                # relY = relY + slotLength
                relX = relX + slotWidth
            elif (labSlot == 5):
                # relX = relX + slotWidth
                # relY = relY + slotLength
                relY = relY + slotLength
                relX = relX + slotWidth
            elif (labSlot == 6):
                # relX = relX + (slotWidth * 2)
                # relY = relY + slotLength
                relX = relX + slotWidth
                relY = relY + (slotLength * 2)
            # Third row
            elif (labSlot == 7):
                # relY = relY + (slotLength * 2)
                relX = relX + (slotWidth * 2)
            elif (labSlot == 8):
                # relX = relX + slotWidth
                # relY = relY + (slotLength * 2)
                relY = relY + slotLength
                relX = relX + (slotWidth * 2)
            elif (labSlot == 9):
                # relX = relX + (slotWidth * 2)
                # relY = relY + (slotLength * 2)
                relY = relY + (slotLength * 2)
                relX = relX + (slotWidth * 2)
            # Forth row
            elif (labSlot == 10):
                # relY = relY + (slotLength * 3)
                relX = relX + (slotWidth * 3)
            elif (labSlot == 11):
                # relX = relX + slotWidth
                # relY = relY + (slotLength * 3)
                relY = relY + slotLength
                relX = relX + (slotWidth * 3)
            elif (labSlot == 12):
                # relX = relX + (slotWidth * 2)
                # relY = relY + (slotLength * 3)
                relY = relY + (slotLength * 2)
                relX = relX + (slotWidth * 3)

        except Exception as e:
            self.ApplicationHandler.LedSetColor("Error")
            print("Warning: Something went wrong in 'calculateAbsoluteXYZ_opentrons()': ", str(e))
            return None
            
        return relX, relY

    """Interface for running Opentrons protocol by given json file"""
    def _runApplication(self, filename):
        try:
            with open(filename) as jf:
                jData = json.load(jf)

            commands = jData["commands"]

            # Create labware handler
            LabwareHandler = LabwareParser(jData)
            # Create command handler
            CommandHandler = CommandParser(jData)

            print(f"Extracted {len(commands)} commands")
            
            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Running")
            self.ApplicationHandler.MoveToSafeOffsetAll()
            self.ApplicationHandler.HomeAllAxises()

            #TODO move to hal layer and provide interface
            self.ApplicationHandler.LedSetColor("Idle")
            print("Start!")

            self._stop_event.clear()
    
            for i in range(len(commands)):
                if not self._stop_event.is_set():
                    self.constructCommand(LabwareHandler, CommandHandler, i)
                else:
                    print("!!!Execution terminated from outside!!!")
                    break

            # self.ApplicationHandler.MoveToSafeOffsetAll()

            
            print("Done!")

        except Exception as e:
            self.ApplicationHandler.LedSetColor("Error")
            print("Warning: Something went wrong in 'runApplication()': ", str(e))

    def runApplication(self, filename):
        if self._execution_thread.is_alive():
            return

        self._execution_thread = threading.Thread(target=self._runApplication, args=(filename, ))
        self._execution_thread.start()

    def stopApplication(self):
        if not self._execution_thread.is_alive():
            return

        self._stop_event.set()
        self._execution_thread.join()

    def waitForApplicationComplete(self):
        if self._execution_thread.is_alive():
            self._execution_thread.join()

    """Prints out all the RAMP settings for given 'axis'"""
    def dumpAxisRampSettings(self, axis):
        self.ApplicationHandler.DumpAxisRampSettings(axis)

def main():
    tester = LiquidHandlerIal()
    tester.runApplication("json_files/21_07_21_17_08-Demo_06_06.json")

if __name__ == '__main__':
    main()

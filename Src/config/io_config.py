import dataclasses
from config.data_types import AnalogModule, PressureControl, UtrasonicSensor
from config.data_types import Vaem
from dataclasses import asdict
from json.decoder import JSONDecodeError
from os import X_OK
from pathlib import Path
from typing import Dict, Any, List
from config import ADAM_CONFIG_FILE, PRESS_CONFIG_FILE, ULTRASONIC_CONFIG_FILE
import json

class AdamEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, AnalogModule):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

class SensorEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UtrasonicSensor):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

class PressEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, PressureControl):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)


#DEF_ADAMS               = ['0', '1', '2']
#DEF_SENSORS             = ['0', '1', '2', '3', '4', '5', '6', '7']
#DEF_PRESSURE_CONTROLLER = ['0', '1']

DEF_ADAMS               = 3
DEF_SENSORS             = 8
DEF_PRESSURE_CONTROLLER = 2


DEF_IP_ADAM = {
    '0': '192.168.0.111',
    '1': '192.168.0.112',
    '2': '192.168.0.113',
}

DEF_PORT_ADAM = {
    '0': 502,
    '1': 502,
    '2': 502,
}

DEF_NAME_ADAM = {
    '0': 'Adam6017',
    '1': 'Adam6024',
    '2': 'Adam6052',
}

DEF_SLAVE_ID_ADAM = {
    '0': 1,
    '1': 1,
    '2': 1,   
}

DEF_READ_ID_PRESS = {
    '0': DEF_NAME_ADAM['2'],
    '1': DEF_NAME_ADAM['2']
}

DEF_CHANNEL_PRESS = {
    '0': 0,
    '1': 1
}

DEF_MAX_VOLT_PRESS = {
    '0': 10,
    '1': 10
}

DEF_MAX_PRESS = {
    '0': 1000,
    '1': 1000
}

DEF_DAC_RESL_PRESS = {
    '0': 4096,
    '1': 4096
}

DEF_SENSITIVITY_SENS = 'LEVEL_C'

DEF_READ_DEV_SENS = {
    '0':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 0
    },
    '1':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 1
    },
    '2':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 2
    },
    '3':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 3
    },
    '4':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 4
    },
    '5':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 5
    },
    '6':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 6
    },
    '7':{
        'id': DEF_NAME_ADAM['1'],
        'channel' : 7
    }
}

DEF_TEACH_DEV_SENS = {
    '0':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 0
    },
    '1':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 1
    },
    '2':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 2
    },
    '3':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 3
    },
    '4':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 4
    },
    '5':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 5
    },
    '6':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 6
    },
    '7':{
        'id': DEF_NAME_ADAM['0'],
        'channel' : 7
    }
}


def _build_adam_config(adamJson: Dict[str, Any]) -> List[AnalogModule]:
    config: Dict[str, Any]
    adamsConfig = []
    config = {}    

    for a in range(0, (len(adamJson) if adamJson else DEF_ADAMS)):
        config = adamJson[a] if adamJson else {}
        adam = AnalogModule(ip=config.get('ip', DEF_IP_ADAM[str(a)]),
                            port=config.get('port', DEF_PORT_ADAM[str(a)]),
                            module=config.get('module', DEF_NAME_ADAM[str(a)]),
                            modbusSlave=config.get('modbusSlave', DEF_SLAVE_ID_ADAM[str(a)])
        )
        adamsConfig.append(adam)

    return adamsConfig


def _build_pressControl_config(pressJson: Dict[str, Any]) -> List[PressureControl]:
    config: Dict[str, Any]
    pressConfig = []
    config = {}

    for p in range(0, len(pressJson) if pressJson else DEF_PRESSURE_CONTROLLER):
        config = pressJson[p] if pressJson else {}

        press = PressureControl(readingDevice=config.get('readingDevice', DEF_READ_ID_PRESS[str(p)]),
                                channel=config.get('channel', DEF_CHANNEL_PRESS[str(p)]),
                                dacResolution= config.get('dacResolution', DEF_DAC_RESL_PRESS[str(p)]),
                                maxPressure= config.get('maxPressure', DEF_MAX_PRESS[str(p)]),
                                maxVoltage=config.get('maxVoltage', DEF_MAX_VOLT_PRESS[str(p)])
        )
        pressConfig.append(press)

    return pressConfig

def _build_sensor_config(sensJson: Dict[str, Any]) -> List[UtrasonicSensor]:
    config: Dict[str, Any]
    sensConfig = []
    config = {}

    for s in range(0, (len(pressJson) if pressJson else DEF_SENSORS)):

        config = sensJson.pop() if sensJson else {}
        press = UtrasonicSensor(readingDevice=config.get('readingDevice', DEF_READ_DEV_SENS[str(s)]),
                                sensitivityLevel=config.get('sensitivityLevel', DEF_SENSITIVITY_SENS),
                                teachingDevice=config.get('teachingDevice', DEF_TEACH_DEV_SENS[str(s)])
        )
        sensConfig.append(press)

    return sensConfig


def _write_config(configs: List, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config

def get_adam_config() -> List[AnalogModule]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(ADAM_CONFIG_FILE)
    config = _build_adam_config(json_config)
    _write_config(config, ADAM_CONFIG_FILE, AdamEncoder)
    return config

def get_ultrasonic_config() -> List[UtrasonicSensor]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(ULTRASONIC_CONFIG_FILE)
    config = _build_sensor_config(json_config)
    _write_config(config, ULTRASONIC_CONFIG_FILE, SensorEncoder)
    return config

def get_pressure_config() -> List[PressureControl]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(PRESS_CONFIG_FILE)
    config = _build_pressControl_config(json_config)
    _write_config(config, PRESS_CONFIG_FILE, PressEncoder)
    return config

if __name__ == "__main__":
    adamJson = _load_config(ADAM_CONFIG_FILE)
    pressJson = _load_config(PRESS_CONFIG_FILE)
    sensJson = _load_config(ULTRASONIC_CONFIG_FILE)
    adam = _build_adam_config(adamJson)
    press = _build_pressControl_config(pressJson)
    sens = _build_sensor_config(sensJson)
    _write_config(adam, ADAM_CONFIG_FILE, AdamEncoder)
    _write_config(press, PRESS_CONFIG_FILE, PressEncoder)
    _write_config(sens, ULTRASONIC_CONFIG_FILE, SensorEncoder)

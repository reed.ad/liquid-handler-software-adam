from dataclasses import asdict, make_dataclass
from json.decoder import JSONDecodeError
from os import X_OK
from pathlib import Path
from typing import Dict, Any, List
import json

from config.data_types import Vaem
from config.data_types import Pgva
from config import PGVA_CONFIG_FILE, VAEM_CONFIG_FILE

class PgvaEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Pgva):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

class VaemEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Vaem):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)



DEF_PGVAS = 2
DEF_VAEMS = 2

DEF_INTERFACE_PGVA = {
    '0': 'tcp/ip',
    '1': 'serial'
}

DEF_BAUDRATE_PGVA = {
    '0': 115200,
    '1': 115200
}

DEF_IP_ADDRESS_PGVA = {
    '0': '192.168.0.118',
    '1': '192.168.0.119'
}

DEF_IP_ADDRESS_VAEM = {
    '0': '192.168.0.118',
    '1': '192.168.0.119',
}

DEF_TCP_PORT_PGVA = {
    '0': 502,
    '1': 502
}

DEF_TCP_PORT_VAEM = {
    '0': 502,
    '1': 502
}


DEF_SERIAL_PORT_PGVA = {
    '0': "/dev/ttyUSB0",
    '1': '/dev/ttyUSB1'
}

DEF_MODBUS_SLAVE_PGVA = {
    '0': 16,
    '1': 16
}

DEF_MODBUS_SLAVE_VAEM = {
    '0': 0,
    '1': 0
}
def _build_vaem_config(vaem_config: Dict[str, Any]) -> List[Vaem]:
    config: Dict[str, Any]
    vaemsConfig = []
    config = {}

    for v in range(0, len(vaem_config) if vaem_config else DEF_VAEMS):
        config = vaem_config[v] if vaem_config else {}

        vaem = Vaem(ip=config.get('ip', DEF_IP_ADDRESS_VAEM[str(v)]),
                    port=config.get('port', DEF_TCP_PORT_VAEM[str(v)]),
                    slave_id=config.get('slave_id', DEF_MODBUS_SLAVE_VAEM[str(v)])
                    )
        vaemsConfig.append(vaem)

    return vaemsConfig

def _build_pv_gen_config(pv_config: Dict[str, Any]) -> List[Pgva]:
    config: Dict[str, Any]
    pgvasConfig = []
    config = {}

    for p in range(0, len(pv_config) if pv_config else DEF_PGVAS):
        config = pv_config[p] if pv_config else {}

        pgva = Pgva(interface=config.get('interface', DEF_INTERFACE_PGVA[str(p)]),
                    baudrate=config.get('baudrate', DEF_BAUDRATE_PGVA[str(p)]),
                    comPort=config.get('comPort', DEF_SERIAL_PORT_PGVA[str(p)]),
                    ip=config.get('ip', DEF_IP_ADDRESS_PGVA[str(p)]),
                    tcpPort=config.get('tcpPort', DEF_TCP_PORT_PGVA[str(p)]),
                    modbusSlave=config.get('modbusSlave', DEF_MODBUS_SLAVE_PGVA[str(p)])                                              
                    )
        pgvasConfig.append(pgva)

    return pgvasConfig


def _write_config(configs: List, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config
    
def get_pg_config() -> List[Pgva]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(PGVA_CONFIG_FILE)
    config = _build_pv_gen_config(json_config)
    _write_config(config, PGVA_CONFIG_FILE, PgvaEncoder)
    return config

def get_vaem_config() -> List[Vaem]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(VAEM_CONFIG_FILE)
    config = _build_vaem_config(json_config)
    _write_config(config, VAEM_CONFIG_FILE, VaemEncoder)
    return config

if __name__ == "__main__":
    jsonConfig1 = _load_config(PGVA_CONFIG_FILE)
    jsonConfig2 = _load_config(VAEM_CONFIG_FILE)
    pgva = _build_pv_gen_config(jsonConfig1)
    vaem = _build_vaem_config(jsonConfig2)
    _write_config(pgva, PGVA_CONFIG_FILE, PgvaEncoder)
    _write_config(vaem, VAEM_CONFIG_FILE, VaemEncoder)

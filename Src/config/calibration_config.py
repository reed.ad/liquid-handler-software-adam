from config.data_types import CalibrationData
from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Dict, Any, List
from config import CALIBRATION_CONFIG_FILE
import json

class CalibrationEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, CalibrationData):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

DEF_CALIBRATIONS = 1

DEF_POINT = [0, 0, 0]

DEF_YAW = 0
DEF_PITCH = 0
DEF_ROLL = 0


def _build_calibration_config(calibJson: Dict[str, Any]) -> CalibrationData:
    return CalibrationData(point1=calibJson.get('point1', DEF_POINT),
                                        point2=calibJson.get('point2', DEF_POINT),
                                        point3=calibJson.get('point3', DEF_POINT),
                                        pitch=calibJson.get('pitch', DEF_PITCH),
                                        roll=calibJson.get('roll', DEF_ROLL),
                                        yaw=calibJson.get('yaw', DEF_YAW))

def _write_config(configs: object, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config

def get_calibratiion_config() -> CalibrationData:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(CALIBRATION_CONFIG_FILE)
    config = _build_calibration_config(json_config)
    #_write_config(config, CALIBRATION_CONFIG_FILE, CalibrationEncoder)
    return config

def set_calibration_config(configuration: CalibrationData):
    """Sets the calibration config to the json file
    """
    _write_config(configuration, CALIBRATION_CONFIG_FILE, CalibrationEncoder)
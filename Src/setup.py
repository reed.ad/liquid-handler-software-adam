from setuptools import setup, find_packages

setup(
    name='v0.0.5 Gantry control',  # Required

    version='0.0.1',  # Required

    description='Software for running protocols and basic machine control',  # Optional

    #long_description=long_description,  # Optional

    long_description_content_type='text/markdown',  # Optional (see note above)

    url='https://gitlab.com/jbruell94/liquid-handler-software/activity',  # Optional

    author='Festo',  # Optional

    author_email='milen.kolev@festo.com',  # Optional

    classifiers=[  # Optional
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Developers',
        'Topic :: Liquid Handling :: hardware control',

        'License :: Apache',

        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],

    keywords='control, liquid handler, setuptools, festo',  # Optional

    #package_dir={'': 'Src'},  # Optional

    packages=find_packages(),  # Required

    python_requires='>=3.8, <4',

    install_requires=['fastapi>=0.7',
                        'uvicorn>=0.15.0',
                        'websockets>=10.1',
                        'pydantic>=1.8.2',
                        'typing>=3.7.4.3',
                        'python-multipart==0.0.5',
                        'jsonschema',
                        'Pymodbus',
                        'TMCL',
                        'PyTrinamic',
                        'Flask'
    ],  # Optionals


    package_data={  # Optional
       'config': ['control_schema.json'],
    },

    #package_data={  # Optional
    #    'sample': ['package_data.dat'],
    #},

    #data_files=[('my_data', ['data/data_file'])],  # Optional

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `sample` which
    # executes the function `main` from this package when invoked:
    #entry_points={  # Optional
    #    'console_scripts': [
    #        'sample=sample:main',
    #    ],
    #},

    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/jbruell94/liquid-handler-software/-/issues',
        'Source': 'https://gitlab.com/jbruell94/liquid-handler-software/-/tree/master',
    },
)
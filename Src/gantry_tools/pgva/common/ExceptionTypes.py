class PgvaNotAvailable(Exception):
    def __init__(self):
        super().__init__('No PGVA availabe. Please connect at least one PGVA!')

class PressureRegulationFailed(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
from dataclasses import dataclass
from typing import List

@dataclass
class Pgva():
    unique_id       : int
    interface       : str
    baudrate        : int
    comPort         : str
    ip              : str
    tcpPort         : int
    modbusSlave     : int

@dataclass
class Dimensions:
    xDimension: float
    yDimension: float
    zDimension: float

@dataclass
class ToolConfig():
    dimensions: Dimensions
    referencePoint: str
    pgvaConfig: List[Pgva]
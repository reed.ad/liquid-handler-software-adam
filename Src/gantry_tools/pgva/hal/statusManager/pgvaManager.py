from typing import List
import asyncio
import time

from hal.statusManager.statusDataTypes import PgvaStatus
from hal import PGVA_HAL_LOG
from hal.pgva_control import PgvaControl

class PgvaControlStatus():
    """
    This is the context class that handles context switches and state methods
    """

    _lq_control_inst: PgvaControl = None
    _status_pg : List[PgvaStatus] = []

    def __init__(self, pgva_control_inst : PgvaControl = None, timeout: int = 0.5, use_asyncio: bool = True):
        self._status_pg = []
        self._status_vc = []
        self._use_asyncio = use_asyncio
        self._timeout = timeout
        self._pgva_control_inst = pgva_control_inst
        self._register_devices()
        
    def _register_devices(self):
        """register devices      
        """
        try:
            for i in range(0, len(self._pgva_control_inst.pgvaModules)):
                self._status_pg.append(PgvaStatus(id=i))
        except Exception as e:
            PGVA_HAL_LOG.info(f'exception {e}')
            PGVA_HAL_LOG.error('Could not register pressure supply devices')


    #def unregister device()
    #         #pass
    @property
    def status_pg(self):
        return self._status_pg 

    async def update_status(self):

        #here we read the status of the PGVA and update the status
        if self._status_pg is not None:
            for i in range(0, len(self._status_pg)):
                self._status_pg[i] = self._pgva_control_inst.get_status_pg(id=self._status_pg[i].id)

    async def run(self):
        while True:
            await self.update_status()
            #print(self.status_pg)
            if self._use_asyncio:
                await asyncio.sleep(self._timeout)
            else:
                time.sleep(self._timeout)

if __name__ == "__main__":
    from hal.pgva_control_sim import SimPgvaControl

    lq = SimPgvaControl()
    man = PgvaControlStatus(lq, use_asyncio=True)


    loop = asyncio.get_event_loop()
    loop.create_task(man.run())

    loop.run_forever()
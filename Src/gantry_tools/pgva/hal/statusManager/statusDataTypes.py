from dataclasses import dataclass
@dataclass
class PgvaStatus():
    id: int = None
    status: str = None

from typing import List, Dict
import asyncio
import uuid

from driver.pgva import PgvaDriver
from hal import PGVA_HAL_LOG
import config.pgva_config as pvConfig
from common.ExceptionTypes import PgvaNotAvailable
from hal.statusManager.statusDataTypes import PgvaStatus

class PgvaControl():
    """This class contains all the interfaces 
    that are needed to control the liquid opperations
    This includes aspirating, dispensing, mixing, tip ejections etc."""
    _instance = None
    _VERSION = "0.0.1"
    _NAME = "PGVA"
    _NO_ERROR_DESCRIPTION = "Normal operation"
    _has_error = False
    _error_description = _NO_ERROR_DESCRIPTION
    _ID = uuid.uuid4()

    def __new__(cls):
        if cls._instance is None:
            PGVA_HAL_LOG.info(f"Initilized Pgva Control class. One instance allowed")
            cls._instance = super(PgvaControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.pgvaModules = []
        try:
            self.toolConfig = pvConfig.get_pg_config()
        except Exception as e:
            PGVA_HAL_LOG.error(f"Failed to get PGVA config: {e}")
            raise

        for config in self.toolConfig.pgvaConfig:
            try:
                module = PgvaDriver(config, PGVA_HAL_LOG)
                self.pgvaModules.append(module)
            except ConnectionError:
                PGVA_HAL_LOG.error("Failed to connect to pressure generation unit")

    def reload_config(self):
        PgvaControl.__init__(self)

    @property
    def version(self):
        return self._VERSION

    @property
    def id(self):
        return self._ID
    
    @property
    def name(self):
        return self._NAME

    @property
    def error_status(self):
        return self._has_error
    
    @property
    def tool_status(self):
        return self._error_description

    @property
    def dimensions(self):
        return self.toolConfig.dimensions.__dict__

    @property
    def referencePoint(self):
        return self.toolConfig.referencePoint

    async def aspirate(self, volume: float, id: int = 1):
        """Makes an aspiration with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected

        @param: volume - volume to aspirate in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotAvailable - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Aspirating from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                pgva_module = self._find_pgva_module(id)
                await pgva_module.aspirate(100, -50)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable

    
    async def dispense(self, volume: float, id: int = 1):
        """Makes a dispense with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected
        @param: volume - volume to dispense in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotAvailable - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Dispensing from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                pgva_module = self._find_pgva_module(id)
                await pgva_module.dispense(100, 50)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable


    async def set_vacuum(self, vacuum: float, id: int = 1):
        """
        @raises: 
            PgvaNotAvailable - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Dispensing from PGVA {id}, volume: {vacuum}')
            #TODO convert from uLiters to volume and openning time
            try:
                pgva_module = self._find_pgva_module(id)
                await pgva_module.setPumpVacuum(vacuum)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable

    async def set_pressure(self, pressure: float, id: int = 1):
        """
        """
        if self.pgvaModules is not None:
            PGVA_HAL_LOG.info(f'Dispensing from PGVA {id}, volume: {pressure}')
            #TODO convert from uLiters to volume and openning time
            try:
                pgva_module = self._find_pgva_module(id)
                await pgva_module.setPumpPressure(pressure)
            except Exception as e:
                self._handle_pgva_exception(e)
        else:
            raise PgvaNotAvailable

    def get_available_devices(self) -> List[Dict[str, str]]:
        available_devices = []
        for module in self.pgvaModules:
            try:
                unique_id = module.pgvaConfig.unique_id
                serial_number = module.read_serial_number()
                # fw_version = module.read_firmware_version()
                available_devices.append({"id": unique_id, "serialNumber": serial_number})
            except Exception as e:
                self._handle_pgva_exception(e)

        return available_devices

    def get_status_pg(self, id: str):
        #read the actual status of the pgva
        # e.g. self.pgvaModules[id].read_status()
        return PgvaStatus(id=id, status='Ready')

    def acknowledge_error(self):
        self._clear_error()

    def _find_pgva_module(self, pgva_id: int):
        try:
            # it is guaranteed that the ID is unique and there will not be more than 1 entry
            return list(filter(lambda x: x.pgvaConfig.unique_id == pgva_id, self.pgvaModules))[0]
        except IndexError:
            raise ValueError("No such PGVA module")
    
    def _set_error_state(self, error_desc):
        self._has_error = True
        self._error_description = error_desc

    def _clear_error(self):
        self._has_error = False
        self._error_description = self._NO_ERROR_DESCRIPTION

    def _handle_pgva_exception(self, exc):
        PGVA_HAL_LOG.error(f"Got exception: {exc.__class__.__name__}({exc})")
        self._set_error_state(str(exc))
        raise

if __name__ == "__main__":
    async def func():
        pg = PgvaControl()
        await pg.aspirate(10, 0)
        await pg.dispense(19, 0)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())

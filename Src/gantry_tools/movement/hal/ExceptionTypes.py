class AxesNotSetupError(Exception):
    def __init__(self):
        super().__init__('Not all axes are setup correctly. Please check connections and reboot!')


class WrongAxisRequestedError(Exception):
    def __init__(self):
        super().__init__('The requested axis in not available!')

class ConfigNotLoadedError(Exception):
    def __init__(self):
        super().__init__('Could not load any configuration. Please check the config directory!')

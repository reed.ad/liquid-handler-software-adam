import asyncio
import uuid
from typing import List
from random import randint

from hal import LOGGER
import config.motion_config as mc
from hal.statusManager.StatusDataTypes import AxisStatus, AxisStatusTypes
from hal.ExceptionTypes import WrongAxisRequestedError

class SimMovementControl():
    """This module contains the basic movement 
    functions availabe for this gantry
    The module uses the asincio package in 
    order not to block the execution while moving
    This must be a singleton class object

    @param: loop- the async event to be used in this module
    """
    _instance = None
    _controller_connected = False
    _axes_status: List[AxisStatus] = []
    _VERSION = "0.0.1"
    _NAME = "MOVEMENT"
    _ID = uuid.uuid4()

    def __new__(cls):
        if cls._instance is None:
            LOGGER.info("creating Movement control class. Using only this instance from now on!")
            cls._instance = super(SimMovementControl, cls).__new__(cls)
        return cls._instance
    
    def __init__(self) -> None:
        self._axisSetup = False
        self.toolConfig = mc.get_config()
        LOGGER.info(f'Loaded the following configuration {self.toolConfig}')
        self.tmcmAxis = {}

    @property
    def axes_status(self):
        return self._axes_status

    @property
    def version(self):
        return self._VERSION

    @property
    def id(self):
        return self._ID

    @property
    def name(self):
        return self._NAME

    @property
    def dimensions(self):
        return self.toolConfig.dimensions.__dict__

    @property
    def referencePoint(self):
        return self.toolConfig.referencePoint

    async def build_axes(self):       
        self._axes_status = []
        try: 
            for config in self.toolConfig.axesConfig:
                self.tmcmAxis[config.name] = config.name
                #add axes while still in Init mode
                self._axes_status.append(AxisStatus(config.name, status=AxisStatusTypes.IDLE))
                await self._configure_axis(self.tmcmAxis[config.name], config.name, config)  
            self._controller_connected = True            
        except Exception as e:
            LOGGER.error(f'Axes cannot be initialized: {e}')
            LOGGER.info('Please check connection, configuration, then reboot and try again!')

    async def _configure_axis(self, tmcm, name: str, axisConfig):
        try:
            if tmcm:                
                LOGGER.info(f'Configured axis {tmcm}')
            else:
                raise ValueError('No axis available')        
        except Exception as e:
            LOGGER.info(f'The error is the following: {e}')
            LOGGER.error('Configuration of Axes Failed. Please check configuration file and try again')

    async def home_axis(self, axis: str):

        """ 
        Homes 1 axis with id
        It uses the stallguard configuration to stop the motors at end point.         
        When the axis is home, it sets the actual possition of the axis to zero
        As a finel step it moves the axis at 20mm from 0

        @param axis:str -> this parameter is dependent on the configuration parameter 'name'
        """
        try:
            if (self._controller_connected == False):
                LOGGER.error('Axis is nost setup. Please try again after successful connection')
                raise Exception("Warning: Action 'HomeAxis()' refused! Axis not setup correctly!")
            #ax = list(filter(lambda x: x.name == axis, self.toolConfig.axesConfig)).pop()
            status = self.get_axis_status_by_id(axis)
            # Setup  one axis at a time
            if axis:
                LOGGER.info(f'Started Homing of axis {axis}')
                status.status = AxisStatusTypes.HOMING

                self._axes_status
                for _ in range(150):
                    await asyncio.sleep(0.1)

                status.status = AxisStatusTypes.IDLE
                status.homed = True
                LOGGER.info(f'Homed axis {axis}')

        except Exception as e:
            LOGGER.error(f'Homing of axis {axis} FAILED!, Please try homing again')
            LOGGER.info(f'Exception :{e}')
            raise WrongAxisRequestedError


    async def jog_axis(self, axis: str, forward: bool):
        """Starts the movement of the given 'axis' in forward or backward direction with velocity set in the configuration
        Velocity for forward and backward could be set with different values
        
        @param: axis - one of the names from the configuration file
        @param: forward - true for moving in the forward direction with positive velocity
        """
        try:
            if (self._controller_connected == False):
                raise Exception(f"Warning: Action 'JogAxis()' refused! Axis {axis} not setup correctly!")
            ax = self.get_axis_by_id(axis)
            status = self.get_axis_status_by_id(axis)
            if ax:
                status.status = AxisStatusTypes.MOVING

                for _ in range(15):                
                    await asyncio.sleep(1)

                status.status = AxisStatusTypes.IDLE

        except Exception as e:
            LOGGER.error(f"Something went wrong (JogAxis({axis})): {e}")
            raise WrongAxisRequestedError

    async def move_axis(self, axis:str, distance_mm: float):
        """
        Move given 'axis' relatively by given 'distance_mm'.
        automatic conversion is done from mm to steps
        @param name: from configuration file
        @param distance_mm: distance to move in mm. Will not move if the distance is larger that 
            that max movement
        """
        try:
            ax = self.get_axis_by_id(axis)

            if self._controller_connected == False:
                LOGGER.error(f"Axis not setup. Cannot move {axis}")
                raise Exception(f"Warning: Action 'MoveAxis()' refused! Axis {axis} not setup correctly!")

            #TODO This idle will check for all three axes together
            status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()
            if status.status == AxisStatusTypes.IDLE and status.homed:
                if((ax.maxMovementmm < distance_mm) or (distance_mm < 0)):
                    LOGGER.error(f'Requested move is beyond the capabilities of the axis {axis}')
                    return
                # Due to always positive value for input distance, multiply by -1 depending on home velocity sign
                if ax.controllerSettings["homeVelocity"] > 0:
                    distance_mm = distance_mm * -1
                status.status = AxisStatusTypes.MOVING
                for _ in range(1000000):                    
                    await asyncio.sleep(0.1)
                
                status.status = AxisStatusTypes.IDLE

                LOGGER.info(f"Move {axis} with:  = {distance_mm} mm")
            else:
                LOGGER.warning(f'Axis {axis} cannot move before homed!')
        except Exception as e:
            LOGGER.error(f"Something went wrong (MoveAxis({axis}, {distance_mm} mm)):", str(e))
            raise WrongAxisRequestedError

    def stop_axis(self, axis:str):
        """Immediately stops given 'axis'"""
        try:
            if (self._controller_connected == False):
                LOGGER.error(f"Warning: Action 'StopAxis()' refused! Axis {axis} not setup correctly!")
                raise Exception(f"Warning: Action 'StopAxis()' refused! Axis {axis} not setup correctly!")

            LOGGER.info(f"Stopped axis {axis}")
        except Exception as e:
            LOGGER.error(f"Something went wrong (StopAxis({axis})):", str(e))
    
    def get_axis_coordinates(self, axis:str) -> float:
        try:
            ax = self.get_axis_by_id(axis)

            if ax is not None:
                coord = randint(1, 1000)

            #LOGGER.info(f'Coordinates of axis {axis} in mm : {str(coord)}')
            return coord

        except Exception as e:
            LOGGER.error(f'Error while reading coordinates on axis {axis} : {e}')
            raise WrongAxisRequestedError
        
    def get_axis_status(self, axis_id: str):
        status = self.get_axis_status_by_id(axis_id)
        status.coordinate = self.get_axis_coordinates(axis_id)
        status.driver_status = self.read_driver_status(axis_id)
        return status
    
    def read_driver_status(self, axis_id: str):
        return 'Normal operation'

    def is_driver_error_active(self):        
        return False

    def get_axis_serial_number(self, axis_id: str):
        axis = self.get_axis_by_id(axis_id)
        if axis:
            serial_number = axis.controllerSettings['id']
        else:
            raise WrongAxisRequestedError
        
        return serial_number

    def get_axis_by_id(self, axis_id: str):
        ax = None
        try:
            ax = list(filter(lambda x: x.name == axis_id, self.toolConfig.axesConfig)).pop()
        except:
            LOGGER.error("Could not find the requested axis")

        return ax

    def get_axis_status_by_id(self, axis_id: str):
        status = None
        try:
            status = list(filter(lambda x: x.name == axis_id, self._axes_status)).pop()
        except:
            LOGGER.error("Could not find the status requested axis")

        return status
    
    async def reload_config(self):
        self.toolConfig = mc.get_config()
        LOGGER.info(f'Reloaded the following configuration {self.toolConfig}')
        await self.build_axes()


"""
async def test():
    m = MovementControl(asyncio.get_event_loop())
    print(m)
    c = MovementControl(asyncio.get_event_loop())
    print(c)
    m.build_axes()
    time.sleep(2)
    await c.home_axis('x')
    await c.move_axis('x', 20)
    LOGGER.info(m.get_axis_coordinates('x'))
    time.sleep(2)
    await c.home_axis('y')
    await c.move_axis('y', 20)
    LOGGER.info(m.get_axis_coordinates('y'))
    time.sleep(2)
    await c.home_axis('z')
    await c.move_axis('z', 20)

    LOGGER.info(m.get_axis_coordinates('z'))

    await c.move_axis('x', 150)
    await c.move_axis('y', 150)
    await c.move_axis('z', 50)

    print('Waiting for homin')
    while 1:
        time.sleep(10)
        LOGGER.info(m.get_axis_coordinates('x'))

asyncio.run(test())
                 
"""


















from dataclasses import dataclass

class AxisStatusTypes():
    IDLE = 'Idle'
    MOVING = 'Moving'
    HOMING = 'Homing'

# @dataclass
# class DriverStatus():
#     status: s

@dataclass
class AxisStatus():
    name: str = None
    homed : bool = False
    status: AxisStatusTypes = None
    coordinate: float = 0.0
    driver_status: str = None


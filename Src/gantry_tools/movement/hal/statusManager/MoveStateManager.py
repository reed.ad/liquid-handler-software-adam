from enum import Enum

class StateRequestsTypes(Enum):
    STOP = 'Stop',
    PAUSE = 'Pause',
    RESUME = 'Resume',
    IDLE = 'Idle'

class MovementStateManager():
    """This handles the stop, pause, resume status of the gantry
    """
    _state_req: StateRequestsTypes = StateRequestsTypes.IDLE

    def __init__(self, name: str, state_req: StateRequestsTypes):
        self._movement_req = state_req
        self.name = name

    @property
    def state(self):
        return self._state_req
        
    @state.setter
    def state(self, state_req: StateRequestsTypes):
        if state_req == StateRequestsTypes.STOP:
            self._state_req = state_req
        elif state_req == StateRequestsTypes.RESUME:
            self._state_req = StateRequestsTypes.IDLE
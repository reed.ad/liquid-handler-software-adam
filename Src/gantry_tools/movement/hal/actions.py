from dataclasses import dataclass, field
from typing import List
import uuid
from copy import deepcopy

@dataclass
class ActionParameter:
    name: str
    type: str

@dataclass
class Action:
    name: str
    description: str
    parameters: List[ActionParameter]
    _id: str = field(default_factory=uuid.uuid4)

ACTIONS = [
    Action(name="home_axis",
            description="Homes one axis",
            parameters=[]),
    Action(name="jog_axis",
            description="Starts the movement of the given 'axis' in forward or backward direction",
            parameters=[ActionParameter(name="forward", type="bool")]),
    Action(name="move_axis",
            description="Move given axis relatively by given 'distance_mm'",
            parameters=[ActionParameter(name="distance_mm", type="float")]),
    Action(name="stop_axis",
            description="Immediately stops given axis.",
            parameters=[]),
    Action(name="resume_axis",
            description="Resumes the normal opperation and removes the stop request.",
            parameters=[]),
    Action(name="get_axis_coordinates",
            description="Returns the coordinates of a given axis",
            parameters=[]),
    Action(name="is_axis_homed",
            description="Returns whether an axis is homed or not",
            parameters=[])
]

def get_actions():
    actions_copy = deepcopy(ACTIONS)
    action_dict_list = [action.__dict__ for action in actions_copy]
    for action in action_dict_list:
        action['parameters'] = [parameter.__dict__ for parameter in action['parameters']]

    return action_dict_list

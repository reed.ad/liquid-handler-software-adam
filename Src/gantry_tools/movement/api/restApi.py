from logging import INFO
from fastapi import FastAPI, HTTPException, WebSocket, File, UploadFile
from websockets import ConnectionClosed
import uvicorn
from typing import List, Dict
import asyncio
import json
import os

from api.schema import *
from api import API_LOGGER
from hal.MoveControl import MovementControl
from hal.SimMoveControl import SimMovementControl
from hal.statusManager.MovementManager import MovementStatus
from hal.actions import get_actions
from config import motion_config

description= """
## Software for using the movement tool

## Use Cases
* Move/Home every axis indipendantly or all together
* Read the status of the tool
"""

tags_metadata = [
    {
        "name": "Status",
        "description": "Status reporting"
    },
    {
        "name": "Actions",
        "description": "Specific actions that can be performed by the tool"
    },
    {
        "name": "Identification",
        "description": "Identification of the tool and devices connected to it"
    },
]

api_ver = "v1.1.0"

MovementHandler : MovementControl = None
StatusHandler : MovementStatus = None

movement = FastAPI(title="Movement Tool", description=description, version=api_ver, openapi_tags=tags_metadata)

@movement.on_event("startup")
async def startup_event():
    global MovementHandler
    global StatusHandler

    try:
        MovementHandler = MovementControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate movement module {e}')

    await MovementHandler.build_axes()

    StatusHandler = MovementStatus(MovementHandler, timeout=0.5, use_asyncio=True)
    t = asyncio.get_event_loop()
    t.create_task(StatusHandler.run())
    
@movement.get("/")
async def index():
    return {"visit": "/docs, /redoc"}

@movement.get("/version_id", tags=['Identification'])
async def get_version():
    """
    Reads the version of the tool driver
    """

    global MovementHandler
    
    return MovementHandler.version

@movement.get("/status/", tags=['Status'])
async def read_status():
    """
    The status of the movement tool
    """
    global MovementHandler
    global StatusHandler

    return Status(error_status=MovementHandler.is_driver_error_active(), tool_status=str(StatusHandler.status))

@movement.get("/who_am_i/", tags=['Identification'])
async def who_am_i():
    """
    Request for tool discovery and identification
    """

    global MovementHandler
    
    return WhoAmI(_id=MovementHandler.id,
                    name=MovementHandler.name,
                    location=os.getenv('APP_PORT'),
                    dimensions=MovementHandler.dimensions,
                    referencePoint=MovementHandler.referencePoint,
                    hardwareDevices=[Device(id=axis, serialNumber=MovementHandler.get_axis_serial_number(axis)) 
                                                                                            for axis in MovementHandler.tmcmAxis],
                    actions=get_actions())

@movement.post("/home/{axis_id}", tags=['Actions'])
async def home_axes(axis_id):
    """
    Home each axis individually by ID.
    """
    global MovementHandler
    global StatusHandler
    try:
        await MovementHandler.home_axis(axis_id)
    except Exception as e:
        raise HTTPException(status_code=404, detail=f"Could not home axis: {e}", headers={"Name": "No compatible name"})

    return "Homed"

@movement.post("/move/{axis_id}", tags=['Actions'])
async def move_axis(axis_id, distance: float):
    """
    Move each axis individually by ID.
    > **Note! Cannot move axes before they are homed!**
    """
    global MovementHandler
    global StatusHandler
    global CalibrationHandler

    point = {}
    transformed_point = []
    try:
        # for ax in Axes.axes:
        #TODO Get the names from the config files...
        # For now move always as absolute
        # if ax.command == AxisCommands.moveAbs:
        """
        point['x'] = MovementHandler.get_axis_coordinates('x')
        point['y'] = MovementHandler.get_axis_coordinates('y')
        point['z'] = MovementHandler.get_axis_coordinates('z')
        print(point)
        point[ax.name] = ax.params.moveDistance
        print(point)
        transformed_point = CalibrationHandler.transform_point([point['x'], point['y'], point['z']])
        print(transformed_point)
        await MovementHandler.move_axis('x', transformed_point[0])
        await MovementHandler.move_axis('y', transformed_point[1])
        await MovementHandler.move_axis('z', transformed_point[2])
        """
        await MovementHandler.move_axis(axis_id, distance)

        # elif ax.command == AxisCommands.moveRel:
        #     currPos = MovementHandler.get_axis_coordinates(ax.name)
        #     MovementHandler.move_axis(ax.name, ax.params.moveDistance + currPos[ax.name])

    except Exception as e:
        API_LOGGER.error(f"{e}")
        raise HTTPException(status_code=404, detail=f"Could not move axis: {e}", headers={"Name": "No compatible name"})

    return f"Moved to {MovementHandler.get_axis_coordinates(axis_id)}"

@movement.post("/jog/{axis_id}", tags=['Actions'])
async def jog_axis(axis_id, direction: MovementDirection):
    """
    Jog each axis individually by ID.
    """
    global MovementHandler
    global StatusHandler

    try:
        #TODO Get the names from the config files...
        await MovementHandler.jog_axis(axis_id, direction == MovementDirection.FORWARD)
    except Exception as e:
        API_LOGGER.error(f"{e}")
        raise HTTPException(status_code=404, detail=f"Could not jog axis: {e}", headers={"Name": "No compatible name"})

    return f"Jogged to {MovementHandler.get_axis_coordinates(axis_id)}"

@movement.post("/stop/{axis_id}", tags=['Actions'])
async def stop(axis_id):
    global MovementHandler

    try:
    #TODO add logic for stopping axes
        MovementHandler.stop_axis(axis_id)
    except RuntimeError as r:
        raise HTTPException(status_code=500, detail=f"Could not stop axes {r}", headers={"Exception": "No compatible name"})

    return "Stopped"

@movement.post("/resume/{axis_id}", tags=['Actions'])
async def resume(axis_id):
    global MovementHandler

    try:
    #TODO add logic for stopping axes
        await MovementHandler.resume_axis(axis_id)
    except Exception as e:
        API_LOGGER.error('could not resume axes')
        raise HTTPException(status_code=404, detail=f"Could not resume axis: {e}", headers={"Name": "No compatible name"})
    
    return "Resumed"
        
@movement.post("/upload_config", tags=['configuration'])
async def create_upload_config_file(file: UploadFile = File(...)):
    global MovementHandler
    
    content = await file.read()
    try:
        temp = content.decode('utf8').replace("'", '"')
        recvJson = json.loads(temp)
    except:
       raise HTTPException(status_code=415, detail="Invalid Input file or empty", headers={'Exception': 'Not compatible configuration'})

    try:
        motion_config.set_config(recvJson)
    except ValueError:
        raise HTTPException(status_code=422, detail="Cannot save file. Incopatible configuration", headers={'Exception': 'Not compatible configuration'})
    except Exception:
        raise HTTPException(status_code=500, detail="Cannot save file. Server error", headers={'Exception': 'Internal server error'})
    
    await MovementHandler.reload_config()
    API_LOGGER.info("New config reloaded")

@movement.post("/get_axis_coordinates/{axis_id}", tags=['Actions'])
async def get_axis_coordinates(axis_id):
    global MovementHandler

    try:
        coordinate = MovementHandler.get_axis_coordinates(axis_id)
    except Exception as e:
        API_LOGGER.error('could not get axis coordinates')
        raise HTTPException(status_code=404, detail=f"Could not get axis coordinates: {e}", headers={"Name": "No compatible name"})

    return f"{coordinate}"

@movement.post("/is_axis_homed/{axis_id}", tags=['Actions'], response_model=bool)
async def is_axis_homed(axis_id):
    global MovementHandler

    try:
        homed_status = MovementHandler.get_axis_status(axis_id).homed
    except Exception as e:
        API_LOGGER.error('could not get axis home status')
        raise HTTPException(status_code=404, detail=f"Could not get axis home status: {e}", headers={"Name": "No compatible name"})

    return homed_status
        
@movement.websocket("/status")
async def websocket_endpoint(websocket: WebSocket):
    global MovementHandler
    global StatusHandler
    
    await websocket.accept()
    
    while True:
        status = Status(error_status=MovementHandler.is_driver_error_active(), tool_status=str(StatusHandler.status))
        try:
            await websocket.send_json(json.dumps(status))
        except ConnectionClosed:
            break
        await asyncio.sleep(1)
    
    
if __name__ == "__main__":

    if os.getenv('APP_PORT'):
        server_port = int(os.environ['APP_PORT'])
    else:
        #set to default
        server_port = 5000

    uvicorn.run("restApi:movement", host="0.0.0.0", port=server_port, log_level=INFO, reload=True)

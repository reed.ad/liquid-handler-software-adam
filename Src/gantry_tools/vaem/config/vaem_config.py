from json.decoder import JSONDecodeError
from pathlib import Path
from typing import Dict, Any, List
import json
import pkg_resources
from jsonschema import validate, ValidationError, draft202012_format_checker

from config.data_types import VaemConfig
from config import VAEM_CONFIG_FILE, CONFIG_LOG


class VaemEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, VaemConfig):
            return obj.__dict__
        # Base class default() raises TypeError:
        return json.JSONEncoder.default(self, obj)

DEF_VAEMS = 2
DEF_IP_ADDRESS_VAEM = '192.168.0.118'
DEF_TCP_PORT_VAEM = 502
DEF_MODBUS_SLAVE_VAEM = 0

def _build_config(vaem_config: Dict[str, Any]) -> List[VaemConfig]:
    config: Dict[str, Any]
    vaemsConfig = []
    config = {}

    for v in range(0, len(vaem_config) if vaem_config else DEF_VAEMS):
        config = vaem_config[v] if vaem_config else {}

        vaem = VaemConfig(ip=config.get('ip', DEF_IP_ADDRESS_VAEM),
                    port=config.get('port', DEF_TCP_PORT_VAEM),
                    slave_id=config.get('slave_id', DEF_MODBUS_SLAVE_VAEM)
                    )
        vaemsConfig.append(vaem)

    return vaemsConfig

def _write_config(configs: List, config_path: Path, encoder):
    with open(config_path, 'w') as file:
        json.dump(configs, file, cls=encoder)

def _load_config(config_path: Path) -> Dict[str, Any]:
    config = {}
    try:
        with open(config_path, 'r') as file:
            config = json.load(file)
    except ImportError:
        print(f"Could not load file{config_path}")
    except JSONDecodeError:
        print(f'decoder error {config_path}')

    return config
    

def get_config() -> List[VaemConfig]:
    """
    Returns a config file based either on default configuration or 
    saved json
    """
    json_config = _load_config(VAEM_CONFIG_FILE)
    config = _build_config(json_config)
    _write_config(config, VAEM_CONFIG_FILE, VaemEncoder)
    return config

def set_config(vaem_cfg: List[VaemConfig]):
    """
    Writes the desired configuration to the config file
    """
    
    try:
        schema = pkg_resources.resource_string('config', 'vaem_schema.json').decode('utf8')
    except FileNotFoundError:
        CONFIG_LOG.critical("Schema resource could not be loaded")
        raise

    try:
        schema_json = json.loads(schema)
        validate(vaem_cfg, schema_json, format_checker=draft202012_format_checker)
        CONFIG_LOG.info(f'Writing file {vaem_cfg}')    
        _write_config(vaem_cfg, VAEM_CONFIG_FILE, VaemEncoder)
    except ValidationError as e:
        CONFIG_LOG.error(f"Incompatible configuration: {e}")
        raise ValueError



if __name__ == "__main__":
    jsonConfig2 = _load_config(VAEM_CONFIG_FILE)
    vaem = _build_config(jsonConfig2)
    _write_config(vaem, VAEM_CONFIG_FILE, VaemEncoder)

from abc import ABC, abstractmethod

class ValveControl(ABC):
    def __init__(self):
        pass
    
    @abstractmethod
    def configure_valves(self, valveOpeningTimes: dict):
        pass

    @abstractmethod
    def open_valve(self):
        pass

    @abstractmethod
    def close_valve(self):
        pass
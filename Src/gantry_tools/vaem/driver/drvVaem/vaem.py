import logging
from pymodbus.client.sync import ModbusTcpClient as TcpClient
import struct
from time import sleep
import asyncio

from config.data_types import VaemConfig
from driver.drvVaem.valve_interface import ValveControl
from driver.drvVaem.vaem_helper import *

readParam = {
    'address' : 0,
    'length' : 0x07,
}

writeParam = {
    'address' : 0,
    'length' : 0x07,
}

def construct_frame(data):
    frame = []
    tmp = struct.pack('>BBHBBQ', data['access'], data['dataType'], data['paramIndex'], data['paramSubIndex'], data['errorRet'], data['transferValue'])
    for i in range(0, len(tmp)-1, 2):
    	frame.append((tmp[i] << 8) + tmp[i+1])
    return frame

def deconstruct_frame(frame):
    data = {}
    if frame is not None:
        data['access'] = (frame[0] & 0xff00) >> 8
        data['dataType'] = frame[0] & 0x00ff
        data['paramIndex'] = frame[1]
        data['paramSubIndex'] = (frame[2] & 0xff00) >> 8
        data['errorRet'] = frame[2] & 0x00ff
        data['transferValue'] = 0
        for i in range(4):
             data['transferValue'] += (frame[len(frame)-1-i] << (i*16))

    return data


class vaemDriver(ValveControl):
    def __init__(self, vaemConfig: VaemConfig, logger: logging):
        self._config = vaemConfig
        self._log = logger
        
        self.client = TcpClient(host=self._config.ip, port=self._config.port)

        for _ in range(5):

            if self.client.connect():
                break
            else:
                self._log.warn(f'Failed to connect VAEM. Reconnecting attempt: {_}')
            if _ == 4:
                self._log.error(f'Could not connect to VAEM: {self._config}')
                raise ConnectionError(f'Could not connect to VAEM: {self._config}')

        self._log.info(f'Connected to VAEM : {self._config}')

        self._vaem_init()

    def _vaem_init(self):
        data = {}
        frame = []
        #set operating mode
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT8.value
        data['paramIndex'] = VaemIndex.OperatingMode.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = VaemOperatingMode.OpMode1.value
        frame = construct_frame(data)
        self.transfer(frame)

        #clear errors
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.ControlWord.value
        data['transferValue'] = VaemControlWords.ResetErrors.value
        frame = construct_frame(data)
        self.transfer(frame)


    def save_settings(self):
        data = {}
        frame = []
        #save settings
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT32.value
        data['paramIndex'] = VaemIndex.SaveParameters.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = 99999
        frame = construct_frame(data)
        self.transfer(frame)

    #read write oppeartion is constant and custom modbus is implemented on top
    def transfer(self, writeData):
        data = 0
        try:
            data = self.client.readwrite_registers(read_address=readParam['address'],read_count=readParam['length'],write_address=writeParam['address'], write_registers=writeData, unit=self._config.slave_id)
            return data.registers
        except Exception as e:
            self._log.error(f'Something went wrong with read opperation VAEM : {e}')

    async def select_valve(self, valve_id:int):
        """Selects one valve in the VAEM. 
        According to VAEM Logic all selected valves can be opened, 
        others cannot with open command

        @param: valve_id - the id of the valve to select

        raises:
            ValueError - raised if the valve id is not supported
        """
        pass
        data = {}
        if(valve_id in range(0, 8)):
            #get currently selected valves
            data = get_transfer_value(VaemIndex.SelectValve, vaemValveIndex[valve_id+1], VaemAccess.Read.value,**{})
            frame = construct_frame(data)
            resp = self.transfer(frame)
            #select new valve
            data = get_transfer_value(VaemIndex.SelectValve, vaemValveIndex[valve_id+1] | deconstruct_frame(resp)['transferValue'], VaemAccess.Write.value,**{})
            frame = construct_frame(data)
            self.transfer(frame)
        else:
            self._log.error(f'opening time must be in range 0-2000 and valve_id -> 0-8')
            raise ValueError

    async def deselect_valve(self, valve_id:int):
        """Deselects one valve in the VAEM. 
        According to VAEM Logic all selected valves can be opened, 
        others cannot with open command

        @param: valve_id - the id of the valve to select. valid numbers are from 1 to 8

        raises:
            ValueError - raised if the valve id is not supported
        """
        pass
        data = {}
        if(valve_id in range(0, 8)):
            #get currently selected valves
            data = get_transfer_value(VaemIndex.SelectValve, vaemValveIndex[valve_id+1], VaemAccess.Read.value,**{})
            frame = construct_frame(data)
            resp = self.transfer(frame)
            #deselect new valve
            data = get_transfer_value(VaemIndex.SelectValve, deconstruct_frame(resp)['transferValue'] & (~(vaemValveIndex[valve_id+1])), VaemAccess.Write.value,**{})
            frame = construct_frame(data)
            self.transfer(frame)
        else:
            self._log.error(f'opening time must be in range 0-2000 and valve_id -> 1-8')
            raise ValueError

    async def configure_valves(self, valve_id: int, opening_time: int):
        """Configure the valves with pre selected parameters"""
        data = {}
        if (opening_time in range(0, 2000)) and (valve_id in range(0, 8)):
            data = get_transfer_value(VaemIndex.ResponseTime, valve_id, VaemAccess.Write.value, **{"ResponseTime" : int(opening_time)})
            frame = construct_frame(data)
            self.transfer(frame)
        else:
            self._log.error(f'opening time must be in range 0-2000 and valve_id -> 1-8')
            raise ValueError


    async def open_valve(self):
        """
        Start all valves that are selected
        """
        data = {}
        #save settings
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.ControlWord.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = VaemControlWords.StartValves.value
        frame = construct_frame(data)        
        self.transfer(frame)

        #reset the control word
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.ControlWord.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = 0
        frame = construct_frame(data)        
        self.transfer(frame)

    async def close_valve(self):
        data = {}
        #save settings
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.ControlWord.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = VaemControlWords.StopValves.value

        frame = construct_frame(data)
        self.transfer(frame)

    def read_status(self):
        """
        Read the status of the VAEM
        The status is return as a dictionary with the following keys:
        -> status: 1 if more than 1 valve is active
        -> error: 1 if error in valves is present
        """
        data = {}
        #save settings
        data['access'] = VaemAccess.Read.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.StatusWord.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = 0

        frame = construct_frame(data)
        resp = self.transfer(frame)
        self._log.info(get_status(deconstruct_frame(resp)['transferValue']))

        return get_status(deconstruct_frame(resp)['transferValue'])

    async def clear_error(self):
        """
        If any error occurs in valve opening, must be cleared with this opperation.
        """
        data  = {}
        data['access'] = VaemAccess.Write.value
        data['dataType'] = VaemDataType.UINT16.value
        data['paramIndex'] = VaemIndex.ControlWord.value
        data['paramSubIndex'] = 0
        data['errorRet'] = 0
        data['transferValue'] = VaemControlWords.ResetErrors.value
        frame = construct_frame(data)
        self.transfer(frame)



if __name__ == "__main__" :
    vaemConfig = VaemConfig('192.168.8.118', 502, 0)

    try:
        vaem = vaemDriver(vaemConfig, logger=logging)
    except Exception as e:
        print(e)
    async def func():
        vaem.init()
        print(vaem.read_status())
        await vaem.select_valve(3)
        print(vaem.read_status())
        await vaem.deselect_valve(3)
        print(vaem.read_status())
        await vaem.select_valve(7)
        print(vaem.read_status())
        await vaem.deselect_valve(7)
        print(vaem.read_status())
        """await vaem.configure_valves(5, 1000)
        while 1:
            sleep(1)
            status = vaem.read_status() 
            if status["Error"] == 1:
                vaem.clear_error()
            sleep(1)
            status = vaem.read_status() 
            await vaem.open_valve()
        """
    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())



    
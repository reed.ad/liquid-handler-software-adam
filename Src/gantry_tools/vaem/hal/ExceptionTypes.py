class VaemNotAvailable(Exception):
    def __init__(self):
        super().__init__('No VAEM Available. Please connect at least one VAEM!')

from dataclasses import dataclass
from typing import Dict, Any

@dataclass
class ValveStatus():
    opening_time: float = 0.0

@dataclass
class VaemStatusType():
    id: int = None
    status: bool = False
    errorStatus: bool = False
    readiness: bool = False
    valves: Dict[str, Any] = None

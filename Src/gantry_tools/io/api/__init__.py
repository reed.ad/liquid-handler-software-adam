import logging
from pathlib import Path
from datetime import datetime

from config import _CONFIG_DIR

_LOGGING_DIR = "lhm_log"
_LOGGING_FILE = 'lhm_api_log'
API_LOGGER = object


def _setup_log(logging_path: Path) -> logging.Logger:
    if not Path(logging_path, _LOGGING_DIR).exists():
        Path.mkdir(Path(logging_path, _LOGGING_DIR))

    if Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).exists():
        Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).unlink()

    Path(logging_path, _LOGGING_DIR, _LOGGING_FILE).touch()
    file = str(Path(logging_path, _LOGGING_DIR, _LOGGING_FILE))

    _handler = logging.FileHandler(file)
    logger = logging.getLogger('api_logger')
    logger.setLevel(logging.INFO)
    logger.addHandler(_handler)
    
    #logging.basicConfig(filename=file, level=logging.DEBUG)
    logger.info(f'Logging Started at {datetime.now()}')
    return logger

API_LOGGER = _setup_log(Path(Path.home(), _CONFIG_DIR))
API_LOGGER.info("Started logging")

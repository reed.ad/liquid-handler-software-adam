from dataclasses import dataclass
from typing import List

@dataclass
class IoConfig():
    id                      : int
    ip                      : str
    port                    : int
    module                  : str
    modbus_slave            : int
    adc_voltage_range       : List[int]
    adc_resolution_bits     : int
    dac_voltage_range       : List[int]
    dac_resolution_bits     : int
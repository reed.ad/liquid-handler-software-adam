from typing import Dict
import asyncio
import time

from hal import IO_LOG
from hal.IoControl import IOControl
from hal.statusManager.StatusDataTypes import DigitalStatus, AnalogStatus

class IOControlStatus():
    """
    This is the context class that handles context switches and state methods
    """

    _io_control_inst    : IOControl = None
    _status_analog      : Dict[int, AnalogStatus] = {}
    _status_digital     : Dict[int, DigitalStatus] = {}

    def __init__(self, io_control_status : IOControl = None,
                 timeout: int = 0.5, use_asyncio: bool = False):
        self._status_analog = {}
        self._status_digital = {}
        self._io_control_inst = io_control_status
        self._timeout = timeout
        self._use_asyncio = use_asyncio
        self._registered_devices = set()

        self._register_devices()
        
    def _register_devices(self):
        """register devices      
        """
        try:
            for adam in self._io_control_inst._adamModules:
                self._add_statuses(adam)
                self._registered_devices.add(adam)
        except Exception as e:
            IO_LOG.info(f'exception {e}')
            IO_LOG.error('Could not register io devices')

    def _add_statuses(self, adam):
        self._status_digital[adam.config.id] = {}
        self._status_digital[adam.config.id]["inputs"] = [0] * adam.module['digInputs']
        self._status_digital[adam.config.id]["outputs"] = [0] * adam.module['digOutputs']
        self._status_analog[adam.config.id] = {}
        self._status_analog[adam.config.id]["inputs"] = [0.0] * adam.module['anInputs']
        self._status_analog[adam.config.id]["outputs"] = [0.0] * adam.module['anOutputs']

    def _remove_statuses(self, adam):
        if adam.config.id in self._status_digital:
            del self._status_digital[adam.config.id]
        
        if adam.config.id in self._status_analog:
            del self._status_analog[adam.config.id]

    @property
    def status_analog(self):
        return self._status_analog 

    @property
    def status_digital(self):
        return self._status_digital

    async def update_status(self):
        current_devices = set()
        for adam in self._io_control_inst._adamModules:
            current_devices.add(adam)
        
        obsolete_devices = self._registered_devices - current_devices
        for item in obsolete_devices:
            self._remove_statuses(item)
            self._registered_devices.remove(item)

        new_devices = current_devices - self._registered_devices
        for item in new_devices:
            self._add_statuses(item)
            self._registered_devices.add(item)

        for adam in self._io_control_inst._adamModules:
            if self._status_digital is not None:
                for i in range(adam.module['digInputs']):
                    self._status_digital[adam.config.id]['inputs'][i] = \
                        await self._io_control_inst.get_digital(adam.config.id, i, "in")
                for i in range(adam.module['digOutputs']):
                    self._status_digital[adam.config.id]['outputs'][i] = \
                        await self._io_control_inst.get_digital(adam.config.id, i, "out")

            if self._status_analog is not None:
                for i in range(adam.module['anInputs']):
                    self._status_analog[adam.config.id]['inputs'][i] = \
                        await self._io_control_inst.get_analog_voltage(adam.config.id, i, "in")
                for i in range(adam.module['anOutputs']):
                    self._status_analog[adam.config.id]['outputs'][i] = \
                        await self._io_control_inst.get_analog_voltage(adam.config.id, i, "out")

    async def run(self):
        while True:
            await self.update_status()
            
            if self._use_asyncio:
                await asyncio.sleep(self._timeout)
            else:
                time.sleep(self._timeout)


if __name__ == "__main__":
    async def func():
        io = IOControl()  
        st = IOControlStatus(io)      
        await io.set_pressure(120, 1)    
        print(io.get_pressure(1))
        await st.update_status()
        print(st.status_pressure[1])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())


            
import asyncio
from typing import Any, List, Dict

#TODO define custon exceptions
from hal.ExceptionTypes import IoNotAvailableError
from hal import IO_LOG
from driver.adam import adamDriver
from config import io_config

class SimIOControl():
    """HAL class that controls input output devices,
    such as digital inputs/outputs and analog inputs/outputs
    """
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            IO_LOG.info(f"Initilized IO Simulation Control class. One instance allowed")
            cls._instance = super(SimIOControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self._adamModules : List[Any] = None
        self._analogSet : Dict[int, Dict[str, List[int]]]
        self._digitalSet : Dict[int, Dict[str, List[int]]]
        try:
            self._adamConfig = io_config.get_config()
            self._adamModules = list(map(lambda x: adamDriver(x, IO_LOG, connect=False), self._adamConfig))
            self._analogSet = {module.config.id: {dir: [0] * num for dir, num in zip(["inputs", "outputs"], \
                 [module.module['anInputs'], module.module['anOutputs']])} for module in self._adamModules}
            self._digitalSet = {module.config.id: {dir: [0] * num for dir, num in zip(["inputs", "outputs"], \
                 [module.module['digInputs'], module.module['digOutputs']])} for module in self._adamModules}

        except ConnectionError:
            IO_LOG('Could not connect to one or all Adams. Please check the configuration or cabling!')
            raise IoNotAvailableError

    def _find_io_module(self, io_id: int):
        try:
            return list(filter(lambda x: x.config.id == io_id, self._adamModules)).pop()
        except IndexError:
            raise ValueError("No such IO module")

    async def set_analog_output_voltage(self, adam_id: int, adam_channel: int, voltage: float):
        adam = self._find_io_module(adam_id)

        if adam.module['anOutputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['anOutputs']:
            raise ValueError("Invalid channel number for this ADAM device")

        adc_value = adam.convertVoltageToIncrements(voltage)
        self._analogSet[adam.config.id]["outputs"][adam_channel] = adc_value

    async def get_analog_voltage(self, adam_id: int, adam_channel: int, type: str) -> float:
        adam = self._find_io_module(adam_id)

        if type == "in":
            if adam.module['anInputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['anInputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            adc_value = self._analogSet[adam.config.id]["inputs"][adam_channel]
        elif type == "out":
            if adam.module['anOutputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['anOutputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            adc_value = self._analogSet[adam.config.id]["outputs"][adam_channel]
        else:
            raise ValueError('"type" must be either "in" or "out"')
        
        return adam.convertIncrementsToVoltage(adc_value, "adc" if type == "in" else "dac")

    async def set_digital_out(self, adam_id: int, adam_channel: int, state: int):
        """
        Set the required digital channels.
        channel -> bit mask for desired channels
        state -> state of the required channels
        e.g. channel = 3, state = 1 => channel 1 is set to 1 and channel 2 is set to 0
        """
        adam = self._find_io_module(adam_id)

        if adam.module['digOutputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['digOutputs']:
            raise ValueError("Invalid channel number for this ADAM device")

        if adam is not None:
            self._digitalSet[adam.config.id]["outputs"][adam_channel] = 1

    async def get_digital(self, adam_id: int, adam_channel: int, type: str):
        """
        Get the state of the digital outputs
        The result is in bit form
        e.g. res = 3 => channels 1 and 2 are ON
        """

        adam = self._find_io_module(adam_id)

        if type == "out":
            if adam.module['digOutputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['digOutputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            diglist = self._digitalSet[adam.config.id]["outputs"][adam_channel]
        elif type == "in":
            if adam.module['digInputs'] == 0 or adam_channel < 0 or adam_channel > adam.module['digInputs']:
                raise ValueError("Invalid channel number for this ADAM device")
            diglist = self._digitalSet[adam.config.id]["inputs"][adam_channel]
        else:
            raise ValueError('"type" must be either "in" or "out"')
        return diglist

    def reload_config(self):
        SimIOControl.__init__(self)


"""
    def LedSetColor(self, opperation:str):

        try:
            if opperation == 'Idle':                
                self.LedHandler.ledStripSet(LedControl.COLORS.FestoColor)
            elif opperation == "Running":
                self.LedHandler.ledStripSet(LedControl.COLORS.RandomColor)
            elif opperation == "Error":
                self.LedHandler.ledStripSet(LedControl.COLORS.ErrorColor)
            else:
                self.LedHandler.ledStripSet(LedControl.COLORS.WhiteColor)
        except Exception as e:
            print(f"Setting Led strip failed {e}")
"""


if __name__ == "__main__":
    async def func():
        io = SimIOControl()
        io.set_digital_out(255, 255)
        print(bin(io.get_digital_out()))
        io.set_analog_output_voltage(0, 0, 2.5)
        result = io.get_analog_input_voltage(0, 0)
        print(result)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())




class IoNotAvailableError(Exception):
    def __init__(self):
        super().__init__('One or all IO is not able to connect!')

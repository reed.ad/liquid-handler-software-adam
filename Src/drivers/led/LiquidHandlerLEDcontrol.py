# SPDX-FileCopyrightText: 2021 ladyada for Adafruit Industries
# SPDX-License-Identifier: MIT

import time
import random
#import board
#import adafruit_dotstar as dotstar

# On-board DotStar for boards including Gemma, Trinket, and ItsyBitsy
#dots = dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, 1, brightness=0.2)

# Using a DotStar Digital LED Strip with 30 LEDs connected to hardware SPI


# Using a DotStar Digital LED Strip with 30 LEDs connected to digital pins
# dots = dotstar.DotStar(board.D6, board.D5, 30, brightness=0.2)

class COLORS:
    RandomColor=1
    ErrorColor=2
    WhiteColor=3
    FestoColor=4
    DynamicColor=5

class LENGHTSTRIP:
    OneSided=72
    TwoSided=144

# HELPERS
# a random color 0 -> 192
def random_color():
    return random.randrange(0, 7) * 32

class LedStrip:
    def __init__(self, lenghtStrip):
        #init the dots peripheral
        #self.dots = dotstar.DotStar(board.SCK, board.MOSI, 144, brightness=0.2)
        self.lenght = lenghtStrip  

    def ledStripSet(self, color):
        try:
            if color == COLORS.RandomColor:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (random_color(), random_color(), random_color())   
                    pass 
            elif color == COLORS.ErrorColor:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (255, 0, 0) 
                    pass   
            elif color == COLORS.WhiteColor:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (255, 255, 255)
                    pass            
            elif color == COLORS.FestoColor:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (0, 100, 255)
                    pass
            elif color == COLORS.DynamicColor:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (0, 0, 255)
                    pass
            else:
                for dot in range(int(self.lenght)):
                    #self.dots[dot] = (0, 0, 0)
                    pass 
        except Exception as e:
            print(f"LED Strip is malfunctioning: {e}")


def main():
    ledHandler = LedStrip(LENGHTSTRIP.OneSided)
    while 1:
        ledHandler.ledStripSet(COLORS.RandomColor)



if __name__ == '__main__':
    main()

"""
        while(1):
            i = 0
            while (i < 10):
                dots[i] = (0,255,255)
                time.sleep(0.1)
                i+=1

            i = 0
            while (i < int(lengthchosen)):
                dots[i] = (0,0,255)
                dots[i+10] = (0,255,255)
                time.sleep(0.1)
                i+=1

            i = int(lengthchosen)
            while(i > (int(lengthchosen) - 10)):
                dots[i] = (0,255,255)
                time.sleep(0.1)
                i-=1

            i = int(lengthchosen)
            while (i > 0):
                dots[i] = (0,0,255)
                dots[i-10] = (0,255,255)
                time.sleep(0.1)
                i-=1
"""  


import time
import json
import logging

MIN_VOLTAGE = 0
MAX_VOLTAGE = 10

sensitivityLevels = {
    "LEVEL_A": {
        "seqNumber": 0,
        "scanRangeMin": 3,
        "scanRangeMax": 150,
    },
    "LEVEL_B": {
        "seqNumber": 1,
        "scanRangeMin": 3,
        "scanRangeMax": 110,
    },
    "LEVEL_C": {
        "seqNumber": 2,
        "scanRangeMin": 3,
        "scanRangeMax": 70,
    },
    "LEVEL_D": {
        "seqNumber": 3,
        "scanRangeMin": 3,
        "scanRangeMax": 30,
    },
}

class baumerDriver():
    def __init__(self, adamModules, config, log: logging):
        self.adamModules = adamModules
        self.config = config
        self._log = log

    def teachSensor(self):
        def timedToggleDigitalOutput(adamModule, channel, timeSec):
            adamModule.writeDigitalChannel(channel, 1)
            time.sleep(timeSec)
            adamModule.writeDigitalChannel(channel, 0)
            time.sleep(0.5)

        teachChannel = self.config["teachingDevice"]["channel"]
        teachDevID = self.config["teachingDevice"]["deviceID"]
        teachDev = self.adamModules[teachDevID]

        self._log.info(f"Factory reset sensor {teachChannel}")
        timedToggleDigitalOutput(teachDev, teachChannel, 6)

        # even though the delay before starting the teach in procedure is not specified in the documentation,
        # if it is less than 2 seconds, the teach-in procedure does not work
        time.sleep(2)
        self._log.info("Pressing for 4 seconds")
        timedToggleDigitalOutput(teachDev, teachChannel, 4)

        # set the desired sensitivity mode
        sensitivitySeqNum = sensitivityLevels[self.config["sensitivityLevel"]]["seqNumber"]
        for _ in range(sensitivitySeqNum):
            self._log.info("Pressing for 250 ms")
            timedToggleDigitalOutput(teachDev, teachChannel, 0.25)

        self._log.info(f"Taught {teachChannel}")
        timedToggleDigitalOutput(teachDev, teachChannel, 2)
        time.sleep(2)
    
    def convertVoltageToDistance(self, voltage):
        sensitivityLevel = sensitivityLevels[self.config["sensitivityLevel"]]
        minDistance = sensitivityLevel["scanRangeMin"]
        maxDistance = sensitivityLevel["scanRangeMax"]

        return minDistance + (voltage - MIN_VOLTAGE) * ((maxDistance - minDistance)/(MAX_VOLTAGE - MIN_VOLTAGE))

    def measure(self):
        readChannel = self.config["readingDevice"]["channel"]
        readDevID = self.config["readingDevice"]["deviceID"]
        readDev = self.adamModules[readDevID]

        analogInput = readDev.readAnalogChannel(readChannel, "in")
        if analogInput is not None: 
            voltageInput = readDev.convertAdcToVoltage(analogInput)

            return self.convertVoltageToDistance(voltageInput)
        else:
            raise ValueError("failed read")


"""
if __name__ == "__main__":
    print("start baumer code")
    
    with open('/home/pi/Liquid-Handler/liquid-handler-software/config/LiquidHandlerFileConfig.json') as jf:
        jData = json.load(jf)

    adamConfigs = jData["adamModules"]

    devices = {}
    for config in adamConfigs:
        devices[config] = adamDriver(adamConfigs[config])

    sensors = {}
    sensorConfigs = jData["baumerSensors"]
    for config in sensorConfigs:
        sensors[config] = baumerDriver(devices, sensorConfigs[config])
    for sensor in sensors.values():
        sensor.teachSensor()


    i = 0
    while 1:
        for sensorName, sensorInstance in sensors.items():
            if i == 6:
                dist = sensorInstance.measure()
                print(f"sensor: {sensorName} -> value {dist}")
                print(f"{sensorName} measured distance: {dist}")
            i = i + 1
        time.sleep(2)
    while 1:
        for sensorName, sensorInstance in sensors.items():
            dist = sensorInstance.measure()
            print(f"sensor: {sensorName} -> value {dist}")
        print(f"{sensorName} measured distance: {dist}")
        time.sleep(2)
"""
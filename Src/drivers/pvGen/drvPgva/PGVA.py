import logging
from pymodbus.client.sync import ModbusTcpClient as TcpClient
from pymodbus.client.sync import ModbusSerialClient as SerialClient
import asyncio

from config.data_types import Pgva as pgvaConfig
class _ModbusCommands:    
#input registers
    VacuumActual=256
    PressureActual=257
    OutputPressureActual=258
    FirmwareVer=259
    FirmwareSubVer=260
    FirmwareBuild=261
    StatusWord=262
    DispenseValveOpenTimeH=263
    DispenseValveOpenTimeL=264
    PumpLifeCntrH=265
    PumpLifeCntrL=266
    PGVALifeCntrH=267
    PGVALifeCntrL=268
    VacuumActualmBar=269
    PressureActualmBar=270
    OutputPressureActualmBar=271
    MinDacIncr=272
    ZeroDacIncr=273
    MaxDacIncr=274
    LastModbusErr=275
    Seed=276
    ManMode=277
#holding registers
    ValveActuationTime=4096
    VacuumThreshold=4097
    PressureThreshold=4098
    OutputPressure=4099
    MinCalibrSetP=4100
    ZeroCalibrSetP=4101
    MaxCalibrSetP=4102
    TcpPort=4108
    UnitId=4109
    VacuumThresholdmBar=4110
    PressureThresholdmBar=4111
    OutputPressuremBar=4112
    ManualTrigger=4113
    DhcpSelect=4115
    StoreEeprom=4196
    ResetPumpLifeCntr=4197
    StartPreCalibration=4198
    AdjustCalibrSetPoints=4199
    AuthStart=4296
    KeyWrite=4297
    ConfigOngoing=4298
#multiple holding registers
    IpAddressMSH=12288
    IpAddressLSH=12289
    GWAdressMSH=12290    
    GWAdressLSH=12291    
    NetMaskMSH=12292
    NetMaskLSH=12293
    MacAddressA=12294
    MacAddressB=12295
    MacAddressC=12296
    ProductionYear=12297
    ProductionDate=12298

class PgvaDriver:
    def __init__(self, pgvaConfig: pgvaConfig, logger: logging):
        self.sensorData = {'vaccumChamber' : 0, 'pressureChamber': 0, 'outputPressure': 0}
        self.pgvaConfig = pgvaConfig
        self.log = logger
         
        if pgvaConfig.interface == 'serial':
            self.client = SerialClient(method="ascii", port=pgvaConfig.comPort, baudrate=pgvaConfig.baudrate)
        elif pgvaConfig.interface == "tcp/ip":
            self.client = TcpClient(host=pgvaConfig.ip, port=pgvaConfig.tcpPort)
        
        for _ in range(5):
            if self.client.connect():
                break
            else:
                self.log.warning(f'Failed to connect PGVA. Reconnecting attempt:, attempt {_}')
            if _ == 4: 
                self.log.error(f'Failed to connect to PGVA {pgvaConfig}')
                raise ConnectionError(f'Could not connect to Pgva {pgvaConfig}')
        
        self.log.info(f'Connected to PGVA {pgvaConfig}')

    def _readData(self, register, sign=True):
        data = 0
        try:
            data = self.client.read_input_registers(register, 1, unit=self.pgvaConfig.modbusSlave)
            return data.registers[0]
        except Exception as e:
            self.log("Error while reading : ", str(e))


    def _writeData(self, register, val, sign=True):
        status = object
        #print(f"{register}, {val}")
        try:
            if val < 0:
                val = val + 2**16
            self.client.write_register(register, val, unit=self.pgvaConfig.modbusSlave)
            status = self.client.read_input_registers(_ModbusCommands.StatusWord, count=1, unit=self.pgvaConfig.modbusSlave) 
            while (status.registers[0] & 1) == 1:
                status = self.client.read_input_registers(_ModbusCommands.StatusWord,count=1, unit=self.pgvaConfig.modbusSlave)
        except Exception as e:
            self.log("error while writing : ", str(e)) 

    #calibration procedure for etting exact levels of P&V
    def _calibration(self):
        #send start calibartion
        self._writeData(_ModbusCommands.StartPreCalibration, 1, sign=True)
        
        #Set max pressure
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 2, sign=False)
        
        #enter actual max
        self._writeData(_ModbusCommands.MaxCalibrSetP, 450, sign=True)

        #Set zero pressure
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 1, sign=False)

        #enter actual 0
        self._writeData(_ModbusCommands.ZeroCalibrSetP, 0, sign=True)
               
        #Set min pressure 
        self._writeData(_ModbusCommands.AdjustCalibrSetPoints, 0, sign=False)
        
        #enter actual min
        self._writeData(_ModbusCommands.MinCalibrSetP, -450, sign=True)
     
    
    def setPumpPressure(self, pressure, vacuum):
        
        #set pressure chamber treshold
        if pressure in range(0, 550):
            self._writeData(_ModbusCommands.PressureThresholdmBar, pressure, sign=True)
        #set vacuum chamber treshold
        if vacuum in range(0, -550):
            self._writeData(_ModbusCommands.VacuumThresholdmBar, vacuum, sign=True)
        

    async def aspirate(self, actuationTime: int, pressure: int):

        #set output pressure
        if pressure in range(-450, 0):
            self._writeData(_ModbusCommands.OutputPressuremBar, pressure)
        else:
            raise ValueError("Pressure Data not in range")
        await asyncio.sleep(0.5)
        #set actuation time
        if actuationTime in range(0, 1000):
            self._writeData(_ModbusCommands.ValveActuationTime, actuationTime, sign=False)
            await asyncio.sleep(actuationTime/1000)
        else:
            raise ValueError("Actuation time not in range")

    async def dispense(self, actuationTime: int, pressure: int):
        #convert from uL to actuation time
        if pressure in range(0, 450):
            self._writeData(_ModbusCommands.OutputPressuremBar, pressure)
        else:
            raise ValueError("Pressure Data not in range")
        await asyncio.sleep(0.5)
        #set actuation time
        if actuationTime in range(0, 1000):
            self._writeData(_ModbusCommands.ValveActuationTime, actuationTime, sign=False)
            await asyncio.sleep(actuationTime/1000)
        else:
            raise ValueError("Actuation time not in range")

    def readSensData(self):

        self.sensorData['vaccumChamber'] = self._readData(_ModbusCommands.VacuumActualmBar, True)
        self.sensorData['pressureChamber'] = self._readData(_ModbusCommands.PressureActualmBar, True)
        self.sensorData['outputPressure'] = self._readData(_ModbusCommands.OutputPressureActualmBar, True)
    
        return self.sensorData        

def main():
    print("starting PGVA")
    pgva = None
    try:
        pgva= PgvaDriver(pgvaConfig(interface='serial', modbusSlave=16, baudrate=115200, comPort="/dev/ttyUSB0", ip="192.168.10.102", tcpPort=8502), logging)
    except:
        logging.error(f"FAILED {pgva}")

if __name__ == '__main__':
    main()


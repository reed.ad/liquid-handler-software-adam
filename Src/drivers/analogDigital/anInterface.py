from abc import ABC, abstractmethod

class analogModules(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def readAllAnalog(self):
        pass

    @abstractmethod
    def writeAllAnalog(self, analogOut):
        pass

    @abstractmethod
    def readAnalogChannel(self, channel):
        pass
    
    @abstractmethod
    def writeAnalogChannel(self, analogOut, channel):
        pass




import asyncio
from typing import List

#TODO define custon exceptions
from hal.ExceptionTypes import AdamNotAvailableError, SensorNotAvailableError, PressureGenerationNotAvailableError
from hal.ioControl import IO_LOG
from drivers.analogDigital.adam import adamDriver
from drivers.sensors.baumer import baumerDriver
from config import io_config
class IOControl():
    """HAL class that controls input output devices,
    such as digital inputs/outputs and analog inputs/outputs
    """
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            IO_LOG.info(f"Initilized IO Control class. One instance allowed")
            cls._instance = super(IOControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self._adamModules : List[adamDriver] = None
        self._sensorModules : List[baumerDriver] = None
        self._pressModules = None
        try:
            self._adamConfig = io_config.get_adam_config()
            self._adamModules = list(map(lambda x: adamDriver(x, IO_LOG), self._adamConfig))
        except ConnectionError:
            IO_LOG.error('Could not connect to one or all Adams. Please check the configuration or cabling!')
            raise AdamNotAvailableError

        if self._adamModules is not None:
            self._sensorConfig = io_config.get_ultrasonic_config()
            self._sensorModules = list(map(lambda x: baumerDriver(self._adamModules, x, IO_LOG), self._sensorConfig))
        else:
            IO_LOG.error('Could not initialize sensors because of connection problems with ADAM devices!')
            raise SensorNotAvailableError
            
        if self._adamModules is not None:
            self._pressConfig = io_config.get_pressure_config()
        else:
            IO_LOG.error('Could not initialize the pressure generation units. No Adam connected!')
            raise PressureGenerationNotAvailableError


    async def get_ultrasonic(self, sensor_id: int):
        """
        Get the sensor measurment by id. 
        Id is the channel of the Adam module. ID is a number in string format
        """    
        if sensor_id in range(0, len(self._sensorModules)):
            return await self._sensorModules[sensor_id].measure()
        else:
            raise ValueError("No such id configured")

    async def set_pressure(self, pressure: float, id: int):
        """
        Set the pressure inside the pressure controller chosen.
        Here the conversion is made from mBar to Volts
        max voltage -> check config file
        max pressure -> check config file
        id -> id of the Vaeb(integer), please check the config for valid indeces
        """
        if id in range(0, len(self._pressConfig)):
            if pressure in range(self._pressConfig[id].maxPressure):
                analogSet = pressure * (self._pressConfig[id].dacResolution/self._pressConfig[id].maxPressure)
                adam = list(filter(lambda x: x.module == self._pressConfig[id].readingDevice, self._adamModules)).pop()
                await adam.writeAnalogChannel(self._pressConfig[id].channel, analogSet)
            else:
                raise ValueError(f'Pressure value out of range {pressure}')
        else:
           raise IndexError("Invalid id")

    async def get_pressure(self, id: int):
        """
        Get the pressure inside the pressure controller chosen.
        Here the conversion is made from Volts to mBar
        max voltage -> check config file
        max pressure -> check config file
        id -> id of the Vaeb(integer), check config for supported indeces
        """
        if id in range(0, len(self._pressConfig)):
            adam = list(filter(lambda x: x.module == self._pressConfig[id].readingDevice, self._adamModules)).pop()
            analogOut = await adam.readAnalogChannel(self._pressConfig[id].channel, "out")
            pressure = analogOut * (self._pressConfig[id].maxPressure/self._pressConfig[id].dacResolution)
            return pressure
        else:
            IndexError("Invalid pressure gen index")


    async def set_digital_out(self, channel, state):
        """
        Set the required digital channels.
        channel -> bit mask for desired channels
        state -> state of the required channels
        e.g. channel = 3, state = 1 => channel 1 is set to 1 and channel 2 is set to 0
        """
        adam = list(filter(lambda x: x.module == "Adam6024", self._adamModules)).pop()
        if adam is not None:
            for i in range(1, 9):
                #print(f"channel : {bin(channel)}, state : {bin(state)}")

                if channel & 0x01:
                    await adam.writeDigitalChannel(i, (state & channel) & 0x01)
                channel = channel >> 1
                state = state >> 1
                #print(f"channel : {bin(channel)}, state : {bin(state)}, i : {i}")


    async def get_digital_out(self):
        """
        Get the state of the digital outputs
        The result is in bit form
        e.g. res = 3 => channels 1 and 2 are ON
        """
        adam = list(filter(lambda x: x.module == "Adam6024", self._adamModules)).pop()
        if adam is not None:
            digOuts = 0
            diglist = adam.readAllDigital()
            for i in range(0, len(diglist)):
                #print(tmp[i])
                if diglist[i]:
                    digOuts = (digOuts | 0x01) << i
            return digOuts


"""
    def LedSetColor(self, opperation:str):

        try:
            if opperation == 'Idle':                
                self.LedHandler.ledStripSet(LedControl.COLORS.FestoColor)
            elif opperation == "Running":
                self.LedHandler.ledStripSet(LedControl.COLORS.RandomColor)
            elif opperation == "Error":
                self.LedHandler.ledStripSet(LedControl.COLORS.ErrorColor)
            else:
                self.LedHandler.ledStripSet(LedControl.COLORS.WhiteColor)
        except Exception as e:
            print(f"Setting Led strip failed {e}")
"""

if __name__ == "__main__":
    async def func():
        io = IOControl()
        await io.set_digital_out(255, 255)
        print(bin(await io.get_digital_out()))

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())



import pytest

from hal.ioControl.IoControl import IOControl
from hal.ExceptionTypes import AdamNotAvailableError

def test_init_no_hardware():
    with pytest.raises(AdamNotAvailableError):
        io_control = IOControl()


if __name__ == "__main__":
    test_init_no_hardware()
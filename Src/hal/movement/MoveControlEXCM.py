import asyncio

from hal.movement import LOGGER
from hal.ExceptionTypes import AxesNotSetupError
from hal.statusManager.MoveStateManager import StateRequestsTypes
from hal.statusManager.StatusDataTypes import AxisStatus, AxisStatusTypes
from MoveControl import MovementControl
import time


MICROSTEP_RESOLUTION = {0:1, 1:2, 2:4, 3:8, 4:16, 5:32, 6:64, 7:128, 8:256}

CURRENT_POSITION = {'x': 0, 'y': 0}

class MovementControlEXCM(MovementControl):
    async def home_axis(self, axis: str):
        try:
            if (self._controller_connected == False):
                LOGGER.error('Axis is nost setup. Please try again after successful connection')
                raise AxesNotSetupError

            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
            status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()

            axis_x = list(filter(lambda x: x.name == "x", self.axesConfig)).pop()
            status_x = list(filter(lambda x: x.name == "x", self._axes_status)).pop()

            axis_y = list(filter(lambda x: x.name == "y", self.axesConfig)).pop()
            status_y = list(filter(lambda x: x.name == "y", self._axes_status)).pop()

            if axis_x and axis_y:
                
                #clear axis errors
                LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')

                if (axis == 'x'):
                    self.tmcmAxis['x'].rotate(-axis_x.controllerSettings["homeVelocity"])
                    self.tmcmAxis['y'].rotate(-axis_y.controllerSettings["homeVelocity"])
                    status_x.status = AxisStatusTypes.HOMING
                    status_y.status = AxisStatusTypes.MOVING
                if (axis == 'y'):
                    self.tmcmAxis['x'].rotate(-axis_x.controllerSettings["homeVelocity"])
                    self.tmcmAxis['y'].rotate(axis_y.controllerSettings["homeVelocity"])
                    status_x.status = AxisStatusTypes.MOVING
                    status_y.status = AxisStatusTypes.HOMING
                
                LOGGER.info(f'Started Homing of axis {axis}')

                while ((self.tmcmAxis['x'].getActualVelocity() != 0 and self.tmcmAxis['x'].getErrorFlags() == 0) and 
                (self.tmcmAxis['y'].getActualVelocity() != 0 and self.tmcmAxis['y'].getErrorFlags() == 0)):
                    await asyncio.sleep(0.1)

                self.tmcmAxis['x'].stop()
                self.tmcmAxis['y'].stop()

                await asyncio.sleep(.5)

                self.tmcmAxis[axis].setActualPosition(0)

                status_x.status = AxisStatusTypes.IDLE
                status_y.status = AxisStatusTypes.IDLE
                status.homed = True

                await self.move_axis(axis, ax.safetyOffset)                

                LOGGER.info(f'Error flags: {self.tmcmAxis[axis].getErrorFlags()}')
                LOGGER.info(f'Load Values: {self.tmcmAxis[axis].getLoadValue()}')
                
                if ax.controllerSettings["encoderAvailable"]:
                    self.tmcmAxis[axis].setEncoderPosition(0)
        

        except Exception as e:
            LOGGER.error(f'Homing of axis {axis} FAILED!, Please try homing again')
            LOGGER.info(f'Exception :{e}')


    async def jog_axis(self, axis: str, forward: bool):
        pass

    async def move_axis(self, axis:str, distance_mm: float):
        ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
        
        status = list(filter(lambda x: x.name == axis, self._axes_status)).pop()
        if not(status.status == AxisStatusTypes.IDLE and status.homed):
            LOGGER.warning(f'Axis {axis} cannot move before homed!')

        if((ax.maxMovementmm < distance_mm) or (distance_mm < 0)):
            LOGGER.error(f'Requested move is beyond the capabilities of the axis {axis}')
            return

        relative_mm = distance_mm - CURRENT_POSITION[axis]

        if (axis == 'x'):
            move1 = asyncio.create_task(self._move_axis_helper('x', relative_mm))
            move2 = asyncio.create_task(self._move_axis_helper('y', relative_mm))
        if (axis == 'y'):
            move1 = asyncio.create_task(self._move_axis_helper('x', relative_mm))
            move2 = asyncio.create_task(self._move_axis_helper('y', -relative_mm))

        await move1
        await move2

        CURRENT_POSITION[axis] = distance_mm

    async def _move_axis_helper(self, axis:str, distance_mm):
        """
        Move given 'axis' relatively by given 'distance_mm'.
        automatic conversion is done from mm to steps
        @param name: from configuration file
        @param distance_mm: distance to move in mm. Will not move if the distance is larger that 
            that max movement
        """
        try:
            ax = list(filter(lambda x: x.name == axis, self.axesConfig)).pop()
            pause_req = list(filter(lambda x: x.name == axis, self._move_req)).pop()
            if self._controller_connected == False:
                LOGGER.error(f"Axis not setup. Cannot move {axis}")
                raise AxesNotSetupError

            if ax.controllerSettings["homeVelocity"] < 0:
                    distance_mm = distance_mm * -1

            impulse_1mm = ax.motorStepsPerRev / ax.fullStep_mm
            impulses = distance_mm * impulse_1mm * (1 << ax.controllerSettings["resolution"])
            self.tmcmAxis[axis].moveTo(self.tmcmAxis[axis].getActualPosition() + int(round(impulses)))
            while self.tmcmAxis[axis].getActualVelocity() != 0:
                if (self.tmcmAxis[axis].getErrorFlags() == 1):
                    LOGGER.warning(f"Warning, stall detected during axis {axis} moving!")
                if pause_req.state == StateRequestsTypes.STOP:
                    LOGGER.info(f'Stopped Axis {axis}. Please Resume it to be able to move again')
                    return
                await asyncio.sleep(0.2)
            LOGGER.info(f"Move {axis} with: {int(impulses)} impulses = {distance_mm} mm")
        except Exception as e:
            LOGGER.error(f"Something went wrong (MoveAxis({axis}, {distance_mm} mm)):", str(e))

    def get_axis_coordinates(self, axis:str) -> float:
        return CURRENT_POSITION[axis]

async def test():
    m = MovementControlEXCM()
    await m.build_axes()
    await m.home_axis('x')
    #await m.home_axis('y')
    await m.move_axis('x', 30)
    await m.move_axis('y', 30)
    await m.move_axis('x', 100)
    await m.move_axis('y', 10)

    """
    LOGGER.info(m.get_axis_coordinates('x'))
    time.sleep(2)
    await c.home_axis('y')
    await c.move_axis('y', 20)
    LOGGER.info(m.get_axis_coordinates('y'))
    time.sleep(2)
    await c.home_axis('z')
    await c.move_axis('z', 20)

    LOGGER.info(m.get_axis_coordinates('z'))

    await c.move_axis('x', 150)
    await c.move_axis('y', 150)
    await c.move_axis('z', 50)

    print('Waiting for homin')
    while 1:
        time.sleep(10)
        LOGGER.info(m.get_axis_coordinates('x'))
    """
if __name__ == "__main__":
    asyncio.run(test())
from typing import Any, Dict
import asyncio

from drivers.pvGen.drvVaem.vaem import vaemDriver
from drivers.pvGen.drvPgva.PGVA import PgvaDriver
from hal.liquidControl import LQ_LOG
import config.pressureGen_config as pvConfig
from hal.ExceptionTypes import PgvaNotAvailabe, VaemNotAvailable
from hal.statusManager.StatusDataTypes import PgvaStatus, VaemStatus

class LiquidControl():
    """This class contains all the interfaces 
    that are needed to control the liquid opperations
    This includes aspirating, dispensing, mixing, tip ejections etc."""
    _instance = None
    def __new__(cls):
        if cls._instance is None:
            LQ_LOG.info(f"Initilized Liquid Control class. One instance allowed")
            cls._instance = super(LiquidControl, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.pgvaModules = None
        self.vaemModules = None
        try:
            self.pgvaConfig = pvConfig.get_pg_config()
            self.pgvaModules = list(map(lambda x: PgvaDriver(x, LQ_LOG), self.pgvaConfig))
        except ConnectionError:
            LQ_LOG.error("Failed to connect to pressure generation unit")
        try:
            self.vaemConfig = pvConfig.get_vaem_config()
            self.vaemModules = list(map(lambda x: vaemDriver(x, LQ_LOG), self.vaemConfig))
        except ConnectionError:
            LQ_LOG.error("Failed to connect to valve control terminal")

    async def aspirate(self, volume: float, id: int = 1):
        """Makes an aspiration with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected

        @param: volume - volume to aspirate in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotPresent - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            LQ_LOG.info(f'Aspirating from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                await self.pgvaModules[id].aspirate(100, -50)
            except ValueError:
                LQ_LOG.error(f'Aspirate time or pressure not in range')
        else:
            raise PgvaNotAvailabe

    
    async def dispense(self, volume: float, id: int = 1):
        """Makes a dispense with volume given in 'uLiters'
        This opperation is only possible if a PGVA is connected
        @param: volume - volume to dispense in [uL]
        @param: id - if more than one PGVA is available
        @raises: 
            PgvaNotPresent - Pgva is not available to use and cannot execute
        """
        if self.pgvaModules is not None:
            LQ_LOG.info(f'Dispensing from PGVA {id}, volume: {volume}')
            #TODO convert from uLiters to volume and openning time
            try:
                await self.pgvaModules[id].dispense(100, 50)
            except ValueError:
                LQ_LOG.error('Dispnese time or pressure is out of range')
        else:
            raise PgvaNotAvailabe

    async def open_valve(self, openning_times: Dict[str, Any], vaem_id: int = 1):
        """
        Open the valves that have the parameter 'opening time' different from 0
        
        @param: vaem_id - string 
        
        raises:
            ValueError - if one of the openning time is incorrect
            IndexError - if vaem id is incorrect
            VeamNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                try:
                    for key, val in openning_times.items():
                        if val['openning_time'] != 0:
                            print(vaem_id, int(key), val['openning_time'])
                            await self.vaemModules[vaem_id].configureValves(int(key), int(val['openning_time']))
                except ValueError as v: LQ_LOG.error(f'Cannot configure valves {v}')

                await self.vaemModules[vaem_id].openValve()
                status = self.vaemModules[vaem_id].readStatus()

                if status["Error"]:
                    pass
                    #TODO Set status in Liquid manager
                else:
                    pass
                    #TODO Set status in Liquid manager                
                #TODO Set valve status in Status manager
                #self._valveStatus[vaemDevice]["valves"] = valves
            else: raise IndexError('Wrong VAEM Index')
        else: raise VaemNotAvailable


    async def clearValveErrors(self, vaem_id: int):
        """
        Clears all the errors on a given VAEM

        @param: vaem_id -> string

        raises:
            IndexError - if the index of the vaem is wrong
            VaemNotAvailable - if no VAEM is configured
        """
        if self.vaemModules is not None:
            if vaem_id in range(0, len(self.vaemConfig)):
                await self.vaemModules[vaem_id].clearError()
            else: raise IndexError
        else: raise VaemNotAvailable

    def get_status_pg(self, id: str):
        #read the actual status of the pgva
        # e.g. self.pgvaModules[id].read_status()
        return PgvaStatus(id=id, status='Ready')

    def get_status_vm(self, id: int):
        status: Dict[str, Any]

        status = self.vaemModules[id].readStatus()
        status.pop('OperatingMode')

        return VaemStatus(id=id, status=status.pop('Status'), 
        errorStatus=status.pop('Error'), readiness=status.pop('Readiness'),
        valves=status)


if __name__ == "__main__":
    async def func():
        valves = {'2': {'openning_time': 1000}}
        lq = LiquidControl()
        await lq.clearValveErrors(0)
        print(lq.get_status_vm(0))
        await lq.open_valve(valves, 0)
        print(lq.get_status_vm(0))
        await lq.aspirate(10, 0)
        await lq.dispense(19, 0)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())

from typing import List
import asyncio

from hal.statusManager.StatusDataTypes import PressureOutStatus, DigitalOutStatus
from hal.ioControl import IO_LOG
from hal.ioControl.IoControl import IOControl


from hal.ioControl.SimIoControl import SimIOControl
class IOControlStatus():
    """
    This is the context class that handles context switches and state methods
    """

    _io_control_inst    : SimIOControl = None
    _status_press       : List[PressureOutStatus] = []
    _status_digOut      : List[DigitalOutStatus] = []

    def __init__(self, io_control_status : SimIOControl = None):
        self._status_press = []
        self._status_digOut = []
        self._io_control_inst = io_control_status
        self._register_devices()
        
    def _register_devices(self):
        """register devices      
        """
        try:
            for pg in self._io_control_inst._pressConfig:
                self._status_press.append(PressureOutStatus(id=self._io_control_inst._pressConfig.index(pg), pressure=0))
            self._status_digOut.append(DigitalOutStatus(digitalOut=0))
        except Exception as e:
            IO_LOG.info(f'exception {e}')
            IO_LOG.error('Could not register io devices')


    #def unregister device()
    #         #pass
    @property
    def status_pressure(self):
        return self._status_press 

    @property
    def status_digital_outs(self):
        return self._status_digOut

    async def update_status(self):

        #here we read the status of the PGVA and update the status
        if self._status_press is not None:
            for pg in self._status_press:
                IO_LOG.info(f'{pg}')
                pg.pressure = self._io_control_inst.get_pressure(pg.id)

        if self._status_digOut is not None:
            self._status_digOut = await self._io_control_inst.get_digital_out()

if __name__ == "__main__":
    async def func():
        io = SimIOControl()  
        st = IOControlStatus(io)      
        await io.set_pressure(120, 1)    
        print(io.get_pressure(1))
        await st.update_status()
        print(st.status_pressure[1])

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())


            
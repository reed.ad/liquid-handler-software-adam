
from hal.statusManager.MovementManager import MovementStatus
from hal.statusManager.LiquidControlManager import LiquidControlStatus
from hal.statusManager.IoControlManager import IOControlStatus
import asyncio
import time


class StatusDaemon():
    """
    Daemon module that handles 
    the update of the statuses of each module
    """
    _mc_manager: MovementStatus = None
    _lq_manager: LiquidControlStatus = None
    _io_manager: IOControlStatus = None
    
    def __init__(self, move_inst: object = None, liquid_inst: object = None, io_inst: object = None,
    timeout:int = 0.5, 
    use_asyncio: bool = False):
        if move_inst:
            self._mc_manager = MovementStatus(move_inst)
        if liquid_inst:
            self._lq_manager = LiquidControlStatus(liquid_inst)
        if io_inst:
            self._io_manager = IOControlStatus(io_inst)
        self._timeout = timeout
        self._use_asyncio = use_asyncio

    async def run(self):
        while True:
            if self._mc_manager:
                await self._mc_manager.update_status()
            if self._lq_manager:
                await self._lq_manager.update_status()
            if self._io_manager:
                await self._io_manager.update_status()
            
            if self._use_asyncio:
                await asyncio.sleep(self._timeout)
            else:
                time.sleep(self._timeout)

if __name__ == "__main__":
    pass
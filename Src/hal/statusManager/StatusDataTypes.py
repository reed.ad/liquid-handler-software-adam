from dataclasses import dataclass
from typing import Dict, Any, List
from enum import Enum

@dataclass
class AxisStatus():
    name: str = None
    homed : bool = False
    status: str = None
    coordinate: float = 0.0

class AxisStatusTypes():
    IDLE = 'Idle'
    MOVING = 'Moving'
    HOMING = 'Homing'

@dataclass
class ValveStatus():
    openning_time: float = 0.0

@dataclass
class VaemStatus():
    id: int = None
    status: bool = False
    errorStatus: bool = False
    readiness: bool = False
    valves: Dict[str, Any] = None

@dataclass
class PgvaStatus():
    id: int = None
    status: str = None

@dataclass
class PressureOutStatus():
    id: int = None
    pressure: float = None

@dataclass
class DigitalOutStatus():
    digitalOut : int = None
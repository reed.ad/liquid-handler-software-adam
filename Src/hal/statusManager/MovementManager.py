from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List
from enum import Enum

from hal.statusManager.StatusDataTypes import AxisStatus
from hal.movement import LOGGER
from hal.movement.MoveControl import MovementControl
class MovementStatus():
    """
    This is the context class that handles context switches and state methods
    """
    _motion_control_instance: MovementControl = None
    _status_ax: List[AxisStatus] = None

    def __init__(self, mv_control_instance):
        self._motion_control_instance = mv_control_instance
        self._status_ax = None
        self._register_axes()

    def _register_axes(self):
        if self._motion_control_instance.axes_status is not None:
            self._status_ax = self._motion_control_instance.axes_status
        else:
            LOGGER.warning('Could not register axes status. Please check configuration files!')

    @property
    def status(self):
        return self._status_ax
    
    async def update_status(self):
        if self._status_ax is not None:
            for i in range(0, len(self._status_ax)):
                self._status_ax[i] = self._motion_control_instance.get_axis_status(self._status_ax[i].name)
                self._status_ax[i].status = str(self._status_ax[i].status)



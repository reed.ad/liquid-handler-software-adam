from typing import List
import asyncio
from copy import deepcopy

from hal.liquidControl.SimLiquidControl import SimLiquidControl

from hal.statusManager.StatusDataTypes import PgvaStatus, VaemStatus
from hal.liquidControl import LQ_LOG
from hal.liquidControl.LiquidControl import LiquidControl

class LiquidControlStatus():
    """
    This is the context class that handles context switches and state methods
    """

    _lq_control_inst: LiquidControl = None
    _status_pg : List[PgvaStatus] = []
    _status_vc : List[VaemStatus] = []

    def __init__(self, liquid_control_inst : LiquidControl = None):
        self._status_pg = []
        self._status_vc = []
        self._lq_control_inst = liquid_control_inst
        self._register_devices()
        
    def _register_devices(self):
        """register devices      
        """
        try:
            for i in range(0, len(self._lq_control_inst.pgvaModules)):
                self._status_pg.append(PgvaStatus(id=i))
            for i in range(0, len(self._lq_control_inst.vaemModules)):
                self._status_vc.append(VaemStatus(id=i, valves=dict()))
        except Exception as e:
            LQ_LOG.info(f'exception {e}')
            LQ_LOG.error('Could not register pressure supply devices')


    #def unregister device()
    #         #pass
    @property
    def status_pg(self):
        return self._status_pg 

    @property
    def status_vc(self):
        return self._status_vc

    async def update_status(self):

        #here we read the status of the PGVA and update the status
        if self._status_pg is not None:
            for i in range(0, len(self._status_pg)):
                self._status_pg[i] = self._lq_control_inst.get_status_pg(id=self._status_pg[i].id)

        if self._status_vc is not None:
            for i in range(0, len(self._status_vc)):
                self._status_vc[i] = self._lq_control_inst.get_status_vm(id=self._status_vc[i].id)

if __name__ == "__main__":
    async def func():
        valves = {x : {"openning_time" : 10} for x in range(0, 8)}
        lq = SimLiquidControl()
        man = LiquidControlStatus(lq)
        await lq.open_valve(valves)
        print(lq._valve_openning_times)
        while 1:
            await man.update_status()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(func())

from typing import List

from drivers.calibration.levelCoordinates import Leveler, PointLocation
from hal.calibration import CALIB_LOG
from hal.movement.MoveControl import MovementControl
import config.calibration_config as cc 

class DeckCalibration():
    """Class that abstracts the calibration procedure.

    The points must be probed one by one
    """
    _config : cc.CalibrationData

    def __init__(self, movement_module: MovementControl):
        self._calibrator = Leveler(CALIB_LOG)
        self._move_inst = movement_module
        self._config = cc.get_calibratiion_config()
        #load default calibration data
        self.load_calibration_data()

    def set_point(self, probe_loc: int):
        """Set one point inside the leveler class 

        @param: probe_loc - location of the probed point in terms of homing
        """
        point: List[float] = []
        location = 0
        try:      
            point.append(self._move_inst.get_axis_coordinates('x'))
            point.append(self._move_inst.get_axis_coordinates('y'))      
            point.append(self._move_inst.get_axis_coordinates('z'))     
            if probe_loc == 1:
                self._config.point1 = point
                location = PointLocation.BOTTOM_LEFT
            elif probe_loc == 2:
                self._config.point2 = point
                location = PointLocation.BOTTOM_RIGHT
            elif probe_loc == 3:
                self._config.point3 = point  
                location = PointLocation.TOP_RIGHT
          

            self._calibrator.probe_point(point, location)
            CALIB_LOG.info(f'probed point {point}')
        except RuntimeError:
            CALIB_LOG.error('Error during Probing')
            raise RuntimeError

    def finish_calibration(self):
        """Finish the calibration and store the rotations

        in the configuration module
        """
        self._calibrator.calculate_rotations()
        self._config.pitch = self._calibrator.pitch
        self._config.roll = self._calibrator.roll
        self._config.yaw = self._calibrator.yaw
        CALIB_LOG.info(f'Pitch: {self._config.pitch}, Roll: {self._config.roll}, Yaw: {self._config.yaw}')
        cc.set_calibration_config(self._config)

    def load_calibration_data(self):
        """Loads the calibration data from configuration into leveler 
        """
        self._calibrator.pitch = self._config.pitch
        self._calibrator.roll = self._config.roll
        self._calibrator.yaw = self._config.yaw
        CALIB_LOG.info(f'Pitch: {self._config.pitch}, Roll: {self._config.roll}, Yaw: {self._config.yaw}')
        
    def transform_point(self, point: List[float]) -> List[float]:
        """Transforms a point using the calibrated rotations
        """
        transormed_p = self._calibrator.transform_point(point)
        CALIB_LOG.info(f'Transformed point from: {point} to: {transormed_p}')
        return transormed_p

if __name__ == "__main__":
    mc = MovementControl()

    calib = DeckCalibration(mc)
    calib._calibrator.probe_point(calib._config.point1, PointLocation.BOTTOM_LEFT)
    calib._calibrator.probe_point(calib._config.point2, PointLocation.BOTTOM_RIGHT)
    calib._calibrator.probe_point(calib._config.point3, PointLocation.TOP_RIGHT)

    calib.finish_calibration()
    point = calib.transform_point([120, 20, 20])
    print(point)
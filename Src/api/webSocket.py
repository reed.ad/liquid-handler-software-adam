from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from pydantic import BaseModel
import json
import asyncio
import time
import uvicorn
from logging import INFO
import jsons

from hal.movement.SimMoveControl import SimMovementControl as MovementControl
from hal.statusManager.StatusDaemon import StatusDaemon
from hal.liquidControl.SimLiquidControl import SimLiquidControl as LiquidControl
from hal.ioControl.SimIoControl import SimIOControl as IOControl
from api import API_LOGGER

class testClass(BaseModel):
    id: int
    data: str

webSocketapp = FastAPI()

@webSocketapp.websocket("/lhsWebSocket")
async def websocket_endpoint(websocket: WebSocket):
    MovementHandler : MovementControl = None
    LiquidHandler : LiquidControl = None
    StatusHandler : StatusDaemon = None
    IoHandler : IOControl = None
    t = asyncio.get_event_loop()
    try:
        MovementHandler = MovementControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate movement module {e}')
    try:
        LiquidHandler = LiquidControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate liquid control module {e}')
    try:
        IoHandler = IOControl()
    except Exception as e:
        API_LOGGER.info(f'Could not instantiate IO module {e}')

    await MovementHandler.build_axes()

    StatusHandler = StatusDaemon(MovementHandler, LiquidHandler, IoHandler, use_asyncio=True)
    t.create_task(StatusHandler.run())

    await websocket.accept()
    try:
        while True:
            time.sleep(2)
            s = {}

            s = {'Movent Status': StatusHandler._mc_manager.status}
            print(s)
            #s.append({'PGVAs Status': StatusHandler._lq_manager.status_pg})
            #s.append({'Valve Controller Status': StatusHandler._lq_manager.status_vc})
            #s.append({'Pressure Control Status': StatusHandler._io_manager.status_pressure})
            #s.append({'Digital Outputs Status': StatusHandler._io_manager.status_digital_outs})
            for status in StatusHandler._mc_manager.status:
                print(jsons.dump(status))
                await websocket.send_json(jsons.dump(status))
    except WebSocketDisconnect:
        print("Disconnected")

if __name__ == "__main__":
    uvicorn.run("webSocket:webSocketapp", host="0.0.0.0", port=6000, log_level=INFO, reload=True)


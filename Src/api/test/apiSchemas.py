from fastapi import params
from pydantic import BaseModel, Field
from typing import Dict, List
from typing import Optional
from enum import Enum


#Axis Control
class AxisStates(BaseModel):
    homed: bool
    moving: bool
    coordinate: float

class AxisStatus(BaseModel):
    axesStatus :  Dict[str, AxisStates]

class AxisCommands(str, Enum):
    moveAbs = "MoveAbsolute"
    moveRel = "MoveRelative"

class AxisParameters(BaseModel):
    moveDistance    : float = Field(
        title="Absolute/Relative movement",
        description="make an absolute/relative movement from homing positon",
        gt=0, #example values take them from config
        lt=500#example values take them from config
    )
    velocity        : float = Field(
        title="Velocity",
        description="Velocity used to move the axis",
        gt=1,       #example values take them from config
        lt=2000     #example values take them from config
    )

class AxisMovementControl(BaseModel):
    name        : str
    command     : AxisCommands
    params      : AxisParameters

class MoveMultipleAxes(BaseModel):
    axes: List[AxisMovementControl] = Field(
        title="List of Axes to control",
        description="Single or a number of axes control settings and parameters",
        max_items=3
    )

class AxisHomeControl(BaseModel):
    name: str

class HomeMultipleAxes(BaseModel):
        axes: List[AxisHomeControl] = Field(
        title="List of Axes to home",
        description="Single or a number of axes homing command",
        max_items=3
    )
#Valve control
class Valve(BaseModel):
    openning_time: float

class ValveControl(BaseModel):
    valves: Dict[int, Valve] = Field(
        title='Valve Control',
        description='Open each valve individually with openning time as a parameter',
    )

#Measure
class Sensor(BaseModel):
    sensor_id   : str
    value       : float

"""
class MultipleSensors(BaseModel):
    sensors : List[Sensor] = Field(
        title='List of Sensors',
        description='Get the measured distance from the sensors required',
    )
"""

#Pressure
class Pressure(BaseModel):
    pressure: float = Field(
        title='Pressure Control',
        description='Set the pressure in mBar',
        lt=2000,
        gt=0
    )

#I/O Control
class DigitalControl(BaseModel):
    digitalOut: int = Field(
        title='Digital Output control',
        description='control digital outputs per 8bit words. Every bit is representing one digital output',
        le=255,
        ge=0
    )

    
class ErrorStates(Enum):
    StatusOK=1
    NotHomed=2
    SensorsMalfunction=3
    VaemMalfunction=4
    MotorMalfunction=5

class ErrorStatus(BaseModel):
    status: str = Field(
        title='General error status',
        description='status could be one of the following\
        Status OK\
        Not Homed\
        Sensors malfunction\
        VAEM malfunction\
        Motor Control Malfunction'
    )

class PressureStatus(BaseModel):
    pressureOutput  : float

class ValveStatus(BaseModel):
    valves : Dict[str, Valve] 
    vaemStatus: str = Field(
        title="VAEM {n} status",
        description="The status of the VAEM {n} valve controllers",
        type=str
    )

class Status(BaseModel):
    axesStatus          : Dict[str, AxisStates] = Field(
        title="List of Axis status",
        description="Single or a number of axes statuses",
    )
    errorStatus         : str
    pressureStatus      : PressureStatus
    valveControl1Status  : ValveStatus
    valveControl2Status  : ValveStatus


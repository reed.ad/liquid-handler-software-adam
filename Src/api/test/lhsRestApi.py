from fastapi import FastAPI, HTTPException
import uvicorn
from typing import IO, List
import asyncio


from apiSchemas import *
#rom hal.liquidHandlerHal import LiquidHandlerHal as liquidHAL
from hal.movement.SimMoveControl import SimMovementControl
from hal.statusManager.StatusDaemon import StatusDaemon
from hal.liquidControl.SimLiquidControl import SimLiquidControl
from hal.ioControl.SimIoControl import SimIOControl

description= """
## Software for usage of liquid handling module

## Use Cases
* Move/Home every axis indipendantly or all together
* Read Status of the module
* Read sensor data from ultrasonic sensors
* Read Digital Input/Output data
* Set pressure output
"""
tags_metadata = [
    {
        "name": "movement",
        "description": "Operations on axes movements. Homing, moving and stopping of axes"
    },
    {
        "name": "input/output",
        "description": "Opperations with input/ouput devices"
    },
    {
        "name": "status",
        "description": "Status reporting"
    },
    {
        "name": "pressure",
        "description": "Pressure control trough VEAB"
    },
    {
        "name": "measure",
        "description": "Measure with ultrasonic sensors"
    },
    {
        "name": "valve control",
        "description": "Measure with ultrasonic sensors"
    },    
]

api_ver = "v1.1.0"
MovementHandler : SimMovementControl = None
LiquidHandler : SimLiquidControl = None
StatusHandler : StatusDaemon = None
IoHandler : SimIOControl = None

lhs = FastAPI(title="Liquid Handling Module", description=description, version=api_ver, openapi_tags=tags_metadata)

@lhs.on_event("startup")
async def startup_event():
    global MovementHandler
    global StatusHandler
    global LiquidHandler
    global IoHandler

    t = asyncio.get_event_loop()
    MovementHandler = SimMovementControl()
    LiquidHandler = SimLiquidControl()
    IoHandler = SimIOControl()

    await MovementHandler.build_axes()

    StatusHandler = StatusDaemon(MovementHandler, LiquidHandler, IoHandler, use_asyncio=True)
    t.create_task(StatusHandler.run())

@lhs.get("/")
async def index():
    return {"visit": "/docs, /redoc"}
"""
@lhs.get("/version", tags=["status"])
async def read_version():
    return ApplicationHandler.version
"""
@lhs.get("/status/", tags=['status'])
async def read_status():
    """
    The status of all the components of the LHM
    """
    global StatusHandler
    s = []

    s.append({'Movent Status': StatusHandler._mc_manager.status}) 
    s.append({'PGVAs Status': StatusHandler._lq_manager.status_pg})
    s.append({'Valve Controller Status': StatusHandler._lq_manager.status_vc})
    s.append({'Pressure Control Status': StatusHandler._io_manager.status_pressure})
    s.append({'Digital Outputs Status': StatusHandler._io_manager.status_digital_outs})

    return s
    #return ApplicationHandler.getStatus()

@lhs.post("/home/", tags=['movement'], response_model=AxisStatus)
async def home_axes(homeCommand: HomeMultipleAxes):
    """
    Home each axis individually or alltogether.
    **The name of the axis can be one of the following:**
    * x
    * y
    * z
    """
    global MovementHandler
    global StatusHandler
    for ax in homeCommand.axes:
        if ax.name in ['x', 'y', 'z']:
            await MovementHandler.home_axis(ax.name)
        else:
            raise HTTPException(status_code=404, detail="no such axis", headers={"Name": "No compatible name"})
    
    return StatusHandler._mc_manager.status


@lhs.post("/move/", tags=['movement'], response_model=AxisStatus)
async def move_axis(Axes: MoveMultipleAxes):
    """
    Move each axis individually or altogether.
    > **Note! Cannot move axes before they are homed!**

    > **Velocity cannot be 0. Velocity is in [mm/s]**

    _The name of the axis can be one of the following:_
    * x
    * y
    * z
    """
    global MovementHandler
    global StatusHandler

    try:
        for ax in Axes.axes:
            #TODO Get the names from the config files...
            if ax.name in ['x', 'y', 'z']:
                if ax.command == AxisCommands.moveAbs:
                    await MovementHandler.move_axis(ax.name, ax.params.moveDistance)
                elif ax.command == AxisCommands.moveRel:
                    currPos = MovementHandler.get_axis_coordinates(ax.name)
                    MovementHandler.move_axis(ax.name, ax.params.moveDistance + currPos[ax.name])
            else:
                raise HTTPException(status_code=404, detail="no such axis", headers={"Name": "No compatible name"})

    except Exception as e:
        print(f"{e}")

    return StatusHandler._mc_manager.status

@lhs.post("/stop/", tags=['movement'], response_model=Status)
async def stop():
    global MovementHandler

    try:
    #TODO add logic for stopping axes
        for x in ['x', 'y', 'z']:
            await MovementHandler.stop_axis(x)
    except:
        print('could not stop axes')


@lhs.get("/measure/{sensor_id}", tags=['measure'], response_model=Sensor)
async def read_sensors(sensor_id: str):
    """
        Return the reqired sensor data by sensor ID[integer]
    """
    global IoHandler
    try:
        sensor_val = await IoHandler.get_ultrasonic(sensor_id=int(sensor_id))
    except ValueError as e:
        raise HTTPException(status_code=404, detail= f'Error: {e}')

    return {'sensor_id': sensor_id, 'value': sensor_val}

@lhs.post("/pressure/{pressure_id}", tags=["pressure"], response_model=PressureStatus)
async def set_pressure(pressureIn: Pressure, pressure_id: int):
    """
        Set the pressure on the VEAB. Expected is in mBar.
    """
    global IoHandler
    global StatusHandler

    await IoHandler.set_pressure(pressureIn.pressure, pressure_id)

    return {"pressureOutput" : StatusHandler._io_manager.status_pressure[pressure_id].pressure}


@lhs.post('/digital/{digital_ch}', tags=['input/output'], response_model=DigitalControl)
async def write_digital(digitalOuts: DigitalControl, digital_ch: int):
    """
    Set the digital output state
    The request is sent as a combination of bitmask and request.

    * digital_ch -> bitmask
    * digitalOut -> state of output

    > The request is in 8 bit chunks
    """
    global IoHandler
    #write digital outputs as an integer bit field
    await IoHandler.set_digital_out(digital_ch, digitalOuts.digitalOut)
    #return status of written outputs
    return {"digitalOut" : await IoHandler.get_digital_out() & digital_ch}

@lhs.get('/digital/{digital_ch}', tags=['input/output'], response_model=DigitalControl)
async def read_digitalOut(digital_ch: int):
    """
    Get the digital output state
    * The response is in 8 bit chunks
    """
    if digital_ch is not None:
        #write digital outputs as an integer bit field
        #return status of written outputs
        #digital channel is a 8 bit mask of the written channels
        return {"digitalOut" : await IoHandler.get_digital_out() & digital_ch}


@lhs.post('/valveControl1/', tags=['valve control'])
async def write_valves_1(vControl: Dict[int, Valve]):
    """
    Set the state of the valves and the opening time of VAEM 1

    > When the openning time is diferent from **0**, the valve will be opened 
    as soon as this request is received with the openning time requested.

    **Note, the pressure is set from the pressure request.**
    """
    global LiquidHandler
    global StatusHandler
    valves = {}
    if vControl is not None:
        #set the opening time and open the valves in the list
        for k, v in vControl.items():
            valves[k] = dict(v)
        await LiquidHandler.open_valve(valves, 0)
    
    return StatusHandler._lq_manager.status_vc

@lhs.post('/valveControl2/', tags=['valve control'])
async def write_valves_2(vControl: Dict[int, Valve]):
    """
    Set the state of the valves and the opening time of VAEM 1

    > When the openning time is diferent from **0**, the valve will be opened 
    as soon as this request is received with the openning time requested.

    **Note, the pressure is set from the pressure request.**
    """
    global LiquidHandler
    global StatusHandler
    valves = {}
    if vControl is not None:
        #set the opening time and open the valves in the list
        for k, v in vControl.items():
            valves[k] = dict(v)
        await LiquidHandler.open_valve(valves, 1)
    
    return StatusHandler._lq_manager.status_vc


if __name__ == "__main__":
    uvicorn.run("lhsRestApi:lhs", host="0.0.0.0", port=5000, log_level=None, reload=True)


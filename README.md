# __Liquid-handler__

## __**Installation instructions**__

Requirements

* Python v3.8 or higher
* fastAPI
* flask
* pymodbus
* RPi.gpio
* Adafruit-Blinka
* adafruit-circuitpython-dotstar
* TMCL
* PyTrinamic
* minimalmodbus
* pydantic
* typing
* uvicorn

All the packages can be installed with pip and requirements.txt

> pip install -r requirements.txt

Software to control handler system.
===


## Features

* Movement of 3 axis gantry
* Aspirate in uL
* Dispense in uL
* Execute complex protocols
* Use Opentrons JSON to execute protocols
* Jupyter Notebook Control
* tbd...

# __Control Functions__

## **Basic Functionalities**

### Movement

--------

```python
from liquidHandlerAPI import LiquidHandlerAPI
```

### __MoveAxis_X__

__*Moves axis **X** with 'position_mm'.*__

> The movement is relative to the current position with velocity set in the configuration file

* distance_mm -> positive values in [mm]

```python
MoveAxis_X(position_mm)
```

### __MoveAxis_Y__

__*Moves axis **Y** with 'position_mm'.*__

> The movement is relative to the current position with velocity set in the configuration file

* distance_mm -> positive values in [mm]

```python
MoveAxis_Y(position_mm)
```

### __MoveAxis_Z__

__*Moves axis **Z** with 'position_mm'.*__

> The movement is relative to the current position with velocity set in the configuration file

* distance_mm -> positive values in [mm]

```python
MoveAxis_Z(position_mm)
```

### __StopAxis_X__

__*Performs immediate stop for axis **X***__

```python
StopAxis_X()
```

### __StopAxis_Y__

__*Performs immediate stop for axis **Y***__

```python
StopAxis_Y()
```

### __StopAxis_Z__

__*Performs immediate stop for axis **Z***__

```python
StopAxis_Z()
```

### __HomeAllAxes__

__*Home all the available axis to home possition with velocity defined in the configuration file.*__

```python
HomeAxises()
```

### Liquid Opperations

--------

### __Aspirate__

__*Performs aspirate from current possition.*__

> The liquid volume equal to 'uLiters'

* uLiters -> amount of liquid to be aspirated

```python
Aspirate(uLiters)
```

### __Dispense__

__**Performs dispense from current possition.**__

> The liquid volume equal to 'uLiters'

* uLiters -> amount of liquid to be dispensed

```python
Dispense(uLiters)
```

### __Mix__

__*Performs mix of liquids.*__

```python
Mix()
```

### Combined Functions

--------

### __MoveTo__

__*Moves all the three axis to given position by 'x_possition', 'y_possition' and 'z_possition'.*__

> First moves 'z' axis to 0 possition, when it is done start moving 'x' and 'y' axises to given possitions, when the possitions are reached it starts moving 'z' axis to given possition

```python
MoveTo(x_possition, y_possition, z_possition)
```

### __AspirateFrom__

__*Moves all the three axis to given position by 'x_possition', 'y_possition' and 'z_possition and performs aspiration with volume given by 'uLiters'.*__

> First moves 'z' axis to 0 possition, when it is done start moving 'x' and 'y' axises to given possitions, when the possitions are reached it starts moving 'z' axis to given possition. When 'z' possitions is reached, it performs aspiration. As a finial move before goes out of the method call it moves 'z' axis back to 0 position

* x_position - position to move in x [mm]
* y_position - position to move in y [mm]
* z_position - position to move in z [mm]
* uLiters - amount of liquid to aspirate

```python
AspirateFrom(x_possition, y_possition, z_possition, uLiters)
```

### __DispenseTo__

__*Moves all the three axis to given position by 'x_possition', 'y_possition' and 'z_possition and performs dispense with volume given by 'uLiters'.*__

> First moves 'z' axis to 0 possition, when it is done start moving 'x' and 'y' axises to given possitions, when the possitions are reached it starts moving 'z' axis to given possition. When 'z' possitions is reached, it performs dispense. As a finial move before goes out of the method call it moves 'z' axis back to 0 position

* x_position - position to move in x [mm]
* y_position - position to move in y [mm]
* z_position - position to move in z [mm]
* uLiters - amount of liquid to dispense

```python
DispenseTo(x_possition, y_possition, z_possition, uLiters)
```

# **Complex Functions**

```python
import liquidHandlerAL
```

### __PickUpTip__

__*Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates' and 'z_coordinates', then performs a pick up tip action*__

> The sequence of movement is as follows:
>
> 1. Positions z axis at 'travel hight' which is configured in the config file.
> 2. Move axis x and y to given coordinates and wait thill the movement is over
> 3. Move axis z to position for picking up a tip
> 4. Move axis z above the well and wait till movement is over

* x_coordinates - x coordinated to pick up tip
* y_coordinates - y coordinated to pick up tip
* z_coordinates - z coordinated to pick up tip
* wellDepth - TBD
* switchLabware - defines if the action is about to move to another labware

```python
pickupTip(x_coordinates, y_coordinates, z_coordinates, wellDepth, switchLabware)
```

### __DropTip__

__*Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates' and 'z_coordinates', then performs a drop tip action.*__

> The sequence of movement is as follows:
>
> 1. Positions z axis at 'travel hight' which is configured in the config file.
> 2. Move axis x and y to given coordinates and wait thill the movement is over
> 3. Move axis z to position for drop a tip
> 4. Move axis z above the well and wait till movement is over

* x_coordinates - x coordinated to drop the tip
* y_coordinates - y coordinated to drop the tip
* z_coordinates - z coordinated to drop the tip
* wellDepth - TBD
* switchLabware - defines if the action is about to move to another labware

```python
dropTip(x_coordinates, y_coordinates, z_coordinates, wellDepth, switchLabware
```

### __Aspirate__

__*Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates', 'z_coordinates','wellDepth' and 'offsetFromBottomMm' then performs a aspirate with liquid volume given by 'liquidVolume'*__

> The sequence of movement is as follows:
>
> 1. Positions z axis at 'travel hight' which is configured in the config file.
> 2. Move axis x and y to given coordinates and wait thill the movement is over
> 3. Move axis z to position for aspirate
> 4. Move axis z above the well and wait till movement is over

* x_coordinates - x coordinated to drop the tip
* y_coordinates - y coordinated to drop the tip
* z_coordinates - z coordinated to drop the tip
* liquidVolume - liquid volume to aspirate
* wellDepth - TBD
* offsetFromBottomMm -  possition for the z axis to be set in respect of offset from the bottom
* switchLabware - defines if the action is about to move to another labware

```python
aspirate(x_coordinates, y_coordinates, z_coordinates, liquidVolume, wellDepth, offsetFromBottomMm, switchLabware)
```

### __Dispense__

__*Position the three axises x, y and z at positions given by 'x_coordinates', 'y_coordinates', 'z_coordinates' 'wellDepth' and 'offsetFromBottomMm' then performs a dispense with liquid volume given by 'liquidVolume'*__

> The sequence of movement is as follows: 
>
> 1. Positions z axis at 'travel hight' which is configured in the config file.
> 2. Move axis x and y to given coordinates and wait thill the movement is over
> 3. Move axis z to position for dispense
> 4. Move axis z above the well and wait till movement is over

* x_coordinates - x coordinated to drop the tip
* y_coordinates - y coordinated to drop the tip
* z_coordinates - z coordinated to drop the tip
* liquidVolume - liquid volume to dispense
* wellDepth - TBD
* offsetFromBottomMm -  possition for the z axis to be set in respect of offset from the bottom
* switchLabware - defines if the action is about to move to another labware

```python
dispense(x_coordinates, y_coordinates, z_coordinates, liquidVolume, wellDepth, offsetFromBottomMm , switchLabware)
```

# __EXAMPLE JUPITER USE__

## *Simple API*

```python
from liquidHandlerAPI import LiquidHandlerAPI

    
def main():
    tester = LiquidHandlerAPI()

    #Home all axes
    tester.HomeAxises()
    
    #Move Axis X to 100mm
    tester.MoveAxis_X(100)

    #Move Axis Y to 100mm
    tester.MoveAxis_Y(100)

    #Move Axis Z to 100mm
    tester.MoveAxis_Z(100)

    #Move All Axes to 20mm
    tester.MoveTo(20, 20, 20)

    #Mix the liquid in this position
    tester.Mix()

    #Aspirate 30uL from location x->10, y->10, z->100
    tester.AspirateFrom(10, 10, 100, 30)

    #Dispense 40uL to location x->40, y->40, z->100
    tester.DispenseTo(40, 40, 100, 40)

if __name__ == '__main__':
    main()
```

# __LiquidHandler Config File__

## *Description of Parameters*

```json
//axis configuration parameters. dictionary object for each axis
"axises": [
        {
            //axis name
            "name": "x",
            //the type of motor controller controlling this axis
            "controllerType": "TMCM_6214"
            //the id used to address this motor
            "id": 2,
            //the id this motor replies to, this is only used when can communication is used
            "replyID": 0,
            //resolution of microstepping. see Trinamic doc (8->divide one step by 256 microsteps)
            "resolution": 8,
            //see Trinamic Doc for more information
            "runCurrent": 128,
            //see Trinamic Doc for more information
            "standbyCurrent": 8,
            //see Trinamic Doc for more information
            "PWMAutoScale": 0,
            //Velocity used during homing procedure
            "homeVelocity": -90800,
            //safety offset from endpoint in [mm]. Moves after successful homing
            "safetyOffset": 20,
            //Sensitivity parameter. See Trinamic Doc for more information
            "StallGuard2Treshold": 1,
            //see Trinamic Doc for more information
            "StopOnStallVelocity": 60000,
            //see Trinamic Doc for more information
            "Stallguard2Filter": 0,
            //[mm] per revolution of this type of axis
            "fullStep_mm": 60,
            //offset used if addOffset is required in move functions [mm]
            "additionalOffset": 0,
            //offset the axis when travelling between labwares
            "travelHightOffsetMm": 0,
            //a point where the left bottom angle of the 1st plate is located according to this axis
            "startingPoint": 14,
            //Velocity when in jog mode in [p/s]
            "jogVelocityForward": 50000,
            //Velocity when in jog mode in [p/s]
            "jogVelocityBackward": -50000,
            //settings for ramp movement. See Trinamic documentation
            "RAMPSettings": 
            {
                "startVelocity": 1,
                "accelerationA1": 240000,
                "velocityV1": 230400,
                "maxAccelerationA2": 500000,
                "maxVelocity": 460800,
                "maxDecelerationD2": 460800,
                "decelerationD1": 240000,
                "stopVelocity": 1,
                "rampWaitTime": 1
            }
        },
        {
            "name": "y",
            "controllerType": "TMCM_6214"
            "id": 3,
            "replyID": 0,
            "resolution": 8,
            "runCurrent": 128,
            "standbyCurrent": 8,
            "PWMAutoScale": 0,
            "homeVelocity": 90800,
            "safetyOffset": 20,
            "StallGuard2Treshold": 2,
            "StopOnStallVelocity": 60000,
            "Stallguard2Filter": 0,
            "fullStep_mm": 60,
            "additionalOffset": 0,
            "travelHightOffsetMm": 0,
            "startingPoint": 439,
            "jogVelocityForward": -50000,
            "jogVelocityBackward": 50000,
            "RAMPSettings": 
            {
                "startVelocity": 1,
                "accelerationA1": 240000,
                "velocityV1": 230400,
                "maxAccelerationA2": 500000,
                "maxVelocity": 460800,
                "maxDecelerationD2": 460800,
                "decelerationD1": 240000,
                "stopVelocity": 1,
                "rampWaitTime": 1
            }
        },
        {
            "name": "z",
            "controllerType": "TMCM_6214"
            "id": 1,
            "replyID": 0,
            "resolution": 8,
            "runCurrent": 128,
            "standbyCurrent": 8,
            "PWMAutoScale": 0,
            "homeVelocity": 90800,
            "safetyOffset": 20,
            "StallGuard2Treshold": 0,
            "StopOnStallVelocity": 60000,
            "Stallguard2Filter": 0,
            "fullStep_mm": 8,
            "additionalOffset": -9,
            "travelHightOffsetMm": 2,
            "startingPoint": 88,
            "jogVelocityForward": -50000,
            "jogVelocityBackward": 50000,
            "RAMPSettings": 
            {
                "startVelocity": 1,
                "accelerationA1": 240000,
                "velocityV1": 230400,
                "maxAccelerationA2": 500000,
                "maxVelocity": 460800,
                "maxDecelerationD2": 460800,
                "decelerationD1": 240000,
                "stopVelocity": 1,
                "rampWaitTime": 1
            }
        }
    ],
    //steps per one revolution
    "impulseFullStep": 200,
    //config section for PGVA
    "pgvaConfig": {
        //type of interface used.
        //1. tcp/ip
        //2. serial
        "interface" : "tcp/ip",
        //in case of serial, the baudrate
        "baudrate" : 115200,
        //bytesize
        "bytesize" : 8,
        //comPort
        "comPort" : "/dev/ttyUSB0",
        //ip address
        "ip" : "192.168.10.102",
        //tcp port
        "tcpPort" : 8502,
        //slave address
        "modbusSlave" : 16
    },
    //DHOP config section
    "dhopConfig": {
        //ip address
        "ip" : "192.168.0.103",
        //tcp port
        "tcpPort" : 8502,
        //slave address
        "modbusSlave" : 16
    },
    //number of deck rows
    "deckRows": 4,
    //number of deck coloumns
    "deckColumns": 3,
    //slot size in X direction [mm]
    "slotSizeX": 86,
    //slot size in Y direction [mm]
    "slotSizeY": 128,
    //border thickness in X direction in [mm]
    "boarderThicknessDirectionX": 4.5,
    //border thickness in Y direction in [mm]
    "boarderThicknessDirectionY": 4.5
}
```
